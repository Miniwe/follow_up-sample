Appointments = new Mongo.Collection('appointments');

Appointments.filter = function (filter) {
  var query = {};

  query.participants = {
    $in: [Meteor.userId()]
  };

  if (filter.today) {
    var date = moment().utcOffset(-7);
    var start_of_day_utc = dateToUTC(new Date(date.startOf('day').format()));
    var end_of_day_utc = dateToUTC(new Date(date.endOf('day').format()));
    query.$and = [
      {"start": {$gte: start_of_day_utc}},
      {"start": {$lt: end_of_day_utc}}
    ]
  }
  if (filter.completed) {
    query.$or = [
      {"status": {$nin: ['pending', 'confirmed']}},
      {"start": {$lt: dateToUTC(new Date(moment().utcOffset(-7).startOf('day').format()))}}
    ];
  }
  if (filter.tomorrow) {
    var tomorrow = moment().add(1, 'day').utcOffset(-7);
    var start_of_day_tomorrow_utc = dateToUTC(new Date(tomorrow.startOf('day').format()));
    var end_of_day_tomorrow_utc = dateToUTC(new Date(tomorrow.endOf('day').format()));
    query.$and = [
      {"start": {$gt: start_of_day_tomorrow_utc}},
      {"start": {$lt: end_of_day_tomorrow_utc}}
    ]
  }
  if (filter.nextweek) {
    var nextMonday = moment().add(1, 'weeks').startOf('isoWeek').utcOffset(-7);
    var start_of_nextweek_utc = dateToUTC(new Date(nextMonday.startOf('day').format()));
    var end_of_nextweek_utc = dateToUTC(new Date(nextMonday.endOf('day').format()));
    query.$and = [
      {"start": {$gt: start_of_nextweek_utc}},
      {"start": {$lt: end_of_nextweek_utc}}
    ]
  }
  return Appointments.find(query, {
    sort: {
      start: -1
    }
  });

};
Schemas = {};
Schemas.Appointments = {};

Schemas.Appointments.Form = new SimpleSchema({
  appointmentType: {
    type: String,
    allowedValues: ['visit', 'surgery', 'other'],
    label: 'Appointment type',
    defaultValue: 'visit',
    autoform: {
      options: 'allowed',
      capitalize: true
    },
    optional: true
  },
  status: {
    type: String,
    allowedValues: ['pending', 'arrived', 'completed', 'scheduled', 'confirmed', 'cancelled', 'did not arrive', 'rescheduled'],
    label: 'Status',
    autoform: {
      options: 'allowed',
      capitalize: true
    },
    autoValue: function () {
      var cancelReason = this.field('cancelReason');
      if (cancelReason.isSet) {
        return 'cancelled';
      }
      else if (!this.value) {
        return 'scheduled';
      }
    },
    optional: true
  },
  start: {
    type: Date,
    label: 'Start time',
    defaultValue: moment().format("YYYY-MM-DDTHH:mm"),
    autoform: {
      type: 'datetime-local',
    }
  },
  end: {
    type: Date,
    label: 'End time',
    custom: function () {
      // get a reference to the fields
      var start = this.field('start');
      var end = this;
      // Make sure the fields are set so that .value is not undefined
      if (start.isSet && end.isSet) {
        if (moment(end.value).isBefore(start.value)) return 'badDate';
      }
    },
    autoform: {
      type: 'datetime-local',
      //timezoneId: 'MST',
      defaultValue: function () {
        if (Meteor.isClient) {
          var startDate = AutoForm.getFieldValue('start', 'insertAppointmentsForm');
          return startDate ? new Date(moment(startDate).add(15, 'm')) : '';
        }
      }
    }
  },
  timezone: {
    type: String,
    label: 'Timezone',
    defaultValue: 'MST'
  },
  comment: {
    type: String,
    label: 'Comment',
    optional: true,
    autoform: {
      rows: 5
    },
    min: 3,
    max: 100,
    custom: function () {
      var cancelReason = this.field('cancelReason');
      var comment = this;
      if (cancelReason.isSet) {
        if (cancelReason.value === 'other' && comment.value.toString().length < 3) {
          return 'badReasonComment'
        }
      }
    }
  },
  referralId: {
    type: String,
    optional: true
  },
  careplanId: {
    type: String,
    label: 'Careplan',
    optional: true,
    autoform: {
      options: function () {
        if (Meteor.isClient) {
          var subject = AutoForm.getFieldValue('subject', 'insertAppointmentsForm');
          return Careplans.find({patientId: subject}).map(function (c) {
            return {label: c._id, value: c._id};
          });
        }
      }
    }
  },
  subject: {
    type: String,
    label: 'Patient name',
    autoform: {
      options: function () {
        return Patients.find({}).map(function (c) {
          return {label: c.name, value: c._id};
        });
      },
      fullTextSearch: true
    }
  },
  participants: {
    type: [String],
    label: 'Physicians',
    autoform: {
      options: function () {
        return Meteor.users.find({}).map(function (c) {
          return {label: c.profile ? c.profile.name : c._id, value: c._id};
        });
      },
      defaultValue: function () {
        return Meteor.userId();
      },
      fullTextSearch: true
    },
    minCount: 1
  },
  cancelReason: {
    type: String,
    allowedValues: ['patient did not show up', 'patient cancelled', 'appointment rescheduled', 'other'],
    label: 'Reason for cancelling appointment',
    autoform: {
      options: 'allowed',
      capitalize: true
    },
    optional: true,
    custom: function () {
      if (this.isUpdate) {
        if (this.isSet) {
          if (this.operator === "$set" && this.value === null || this.value === "") return "required";
          if (this.operator === "$unset") return "required";
          if (this.operator === "$rename") return "required";
        }
      }
    }
  },
  // location: {
  //   type: String,
  //   allowedValues: [LT_CLINIC, LT_HOSPITAL],
  //   autoform: {
  //     options: 'allowed',
  //     capitalize: true
  //   },
  //   optional: true
  // },
  location: {
      type: LocationFormSchema,
      label: 'Location',
      optional: false
  },
  encounterSubtypeId: {
    type: String,
    label: 'Encounter subtype',
    optional: true,
    autoform: {
      options: function () {
        if (Meteor.isClient) {
          var location = AutoForm.getFieldValue('location.type', 'insertAppointmentsForm');
          var type = AutoForm.getFieldValue('appointmentType', 'insertAppointmentsForm');
          if (location) {
            var subtypes = new EncounterType(location).getSubtypes(type);
            if (subtypes) {
              return subtypes.map(function (c) {
                return {label: c, value: c};
              });
            }
          }
        }
      }
    }
  }
});

Appointments.attachSchema(Schemas.Appointments.Form);

SimpleSchema.messages({
  badDate: 'End date must be after the start date.',
  notDateCombinationUnique: 'The start/end date combination must be unique',
  badReasonComment: 'Other reason must be at least of 3 characters'
});

function dateToUTC(date) {
  return new Date(
    date.getUTCFullYear(),
    date.getUTCMonth(),
    date.getUTCDate(),
    date.getUTCHours(),
    date.getUTCMinutes(),
    date.getUTCSeconds());
}
