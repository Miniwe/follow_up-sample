// careplan status consts - todo: in far will be used anyway - now only for this file
CS_INACTIVE     = 'inactive';
CS_ACTIVE       = 'active';
CS_NEW          = 'new';
CS_NOT_STARTED  = 'not started';
CS_PLANNED      = 'planned';
CS_INPROGRESS   = 'inprogress';
CS_IN_PROGRESS  = 'in-progress';
CS_OVERDUE      = 'overdue';
CS_COMPLETED    = 'completed';

Careplans = new Mongo.Collection("careplans", {
  transform: function (item) {
    return new Careplan(item);
  }
});

Careplans.filter = function (filter = {}) {
  var query = {};

  if (filter.myCareplans) {
    query.presidingPhysician = Meteor.userId();
  }

  if (filter.type) {
    query.carePathwayStatus = {
      $in: filter.type
    };
  }
  return Careplans.find(query, {
    sort: {
      updatedDate: -1
    }
  });
}

CareplanDiagnosis = new Mongo.Collection("careplan.diagnosis");
CareplanDiagnosis.attachSchema(CareplanDiagnosisSchema);

class CareplanStore {
  constructor() {
    Register = () => {
      switch (Action.type()) {
        case 'CAREPLANS':
          return Careplans.find({});
        default:
          return Action.payload();
      }
    };
  }
}

CareplanStore = new CareplanStore();

Careplan = function (doc) {
  _.extend(this, doc);

  // this.isActive = doc.isActive || true;
  this.patient = Patients.findOne({
    _id: this.patientId
  });
  var questionnaires = this.questionnaires = Questionnaires.find({
    careplanId: this._id
  }, {
    sort: {
      createdDate: -1
    }
  }).fetch();
}

Careplan.prototype.isLocked = function () {
  var careplan = this;
  var ica = this.getICA();
  if (ica && !ica.completedBy) {
    return true;
  }
  return false;
}
Careplan.prototype.needsTreatment = function () {
  // var careplan = this;
  // var ica = this.getICA();
  // if (!this.treatmentOptionId &&
  //   ica &&
  //   ica.completedBy) {
  //   return true;
  // }
  return false;
}


Careplan.prototype.diagnosePatient = function () {
  var ica = this.getICA();
  if (ica) {
    var form = InitialClinicalAssessments.findOne({
      _id: ica._id
    });
    if (form) {
      var value = this.getClinicalCategory(form.spine_upperClinical, form.spine_lowerClinical);
      if (this.diagnose !== diagnose) {
        this.diagnose = diagnose;
        this.save();
      }
      return value;
    }
  }
}

// Careplan.prototype.isActive = function (questionnaire) {
//   return this.getActiveQuestionnaires().indexOf(questionnaire) > -1;
// }

Careplan.prototype.getActiveQuestionnaires = function () {
  var careplan = this;
  var questionnaires = this.questionnaires;
  var active = undefined;
  if (questionnaires) {
    for (questionnaire of questionnaires) {
      if (!questionnaire.completedBy && !active) {
        active = questionnaire;
      }
    }
  }
  return active ? [active] : [];
}
Careplan.prototype.getTreatmentOption = function () {
  return TreatmentOptions.findOne({
    name: "Continue to follow as inpatient, disposition to be determined"
  });
  // if (this.treatmentOptionId) {
  // }
  // return false;
};
Careplan.prototype.getTreatmentType = function () {
  var option = this.getTreatmentOption();
  if (option) {
    return option.getType();
  }
};

Careplan.prototype.getICA = function () {
  var ica = Questionnaires.findOne({
    careplanId:  this._id,
    type: "InitialClinicalAssessment"
  });
  return ica;
};

Careplan.prototype.process = function () {

    //might have to remove this altogether and just assign the patient profile page.
    // once checkedin/admitted,  we assign the patient the intake form?
    //we take the values stored in the medicalhistory file and transfer it to the patient_ica form

  // var option,
  //     ica = this.getICA();
  // // if (Meteor.isServer) {
  //   if (ica) {
  //     if (option = this.getTreatmentOption()) {
  //       let forms = option.getForms();
  //       var form = InitialClinicalAssessments.findOne({
  //         _id: ica._id
  //       });

  //       if (form && (form.PP1 == 5 || form.CA1A == 4 || form.CA1B == 4)) {
  //         let asia = Forms.findOne({
  //           type: "ASIASpinalExam"
  //         });
  //         forms.push(asia);
  //       }
  //       this._saveForms(forms);
  //     }
  //   }
  //   else {
  //     this._saveForms([Forms.findOne({
  //       type: "InitialClinicalAssessment"
  //     })]);

  //     let initFormData = {
  //         careplanId: this._id,
  //         patientId: this.patientId,
  //         assignedTo: [this.patientId]
  //     };
  //     //might have to remove this altogether and just assign the patient profile page.
  //     // once checkedin/admitted,  we assign the patient the intake form?
  //     //we take the values stored in the medicalhistory file and transfer it to the patient_ica form
  //     (new Questionnaire(initFormData)).assignForm('PatientInitialInterview').save();
  //     //(new Questionnaire(initFormData)).assignForm('PatientConsent').save();
  //   }
  //   // forms = forms.concat(option === undefined ? [] : option.getForms());
  // // }
}

Careplan.prototype._saveForms = function (forms) {

  if (this.isLocked()) {
    return;
  }
  var item = {
    status: CS_PLANNED, //planned, in-progress, Completed, Overdue, onhold, Cancelled
    approvedBy: this.createdBy,
    priority: "low",
    dueDate: moment().endOf('day').toDate(), //date when it must be completed, same date as the task
    careplanId: this._id,
    subject: this.patientId,
    name: this.name,
    patientId: this.patientId,
    assignedTo: [this.createdBy],
    createdBy: this.createdBy,
    //all of the questions within the questionnaire
    isActive: this.isActive
      //statusHistory: [status, period]  historical record of
  };

  for (treatmentForm of forms) {
    if (treatmentForm) {
      var questionnaire = Questionnaires.findOne({
        formId: treatmentForm._id,
        careplanId: this._id
      });
      if (!questionnaire) {
        var qItem = _.extend({}, item, treatmentForm, {
              _id: Random.id(),
              formId: treatmentForm._id
            });
        delete qItem['save'];
        (new Questionnaire(qItem)).save();
      }
    }
  }
}

Careplan.prototype.getStatus = function (val) {
  var item = this;
  var result = val || CS_PLANNED;

  var ica = this.getICA();
  var treatmentType = this.getTreatmentType();
  if (ica) {
    result = ica.completedBy ? CS_IN_PROGRESS : CS_ACTIVE;
    if (treatmentType) {
      switch (treatmentType.name) {
        case ttTypes[TT_NO_REQUIRED] || ttTypes[TT_DISCHAGRE] || ttTypes[TT_ANOTHER]:
          result = CS_COMPLETED
          break;
      }
    }
  }
  // if (item.carePathwayStatus === PS_DISCHARGE_NO_FURTHER_TREATMENT_REQUIRED) {
  //   result = 'completed';
  // }
  return result;
}
Careplan.prototype.getEncounter = function () {
  return Encounters.findOne({
    careplanId: this._id
  });
}
Careplan.prototype.getUrl = function () {
  return "/careplans/" + this._id;
}
Careplan.prototype.saveEncounter = function () {
  let tmpLocation = {
          type: 'hospital',
          name: 'tmp name'
      },
      encoutnerItem = {
            _id: Random.id(),
            careplanId: this._id,
            patientId: this.patientId,
            subject: this.patientId,
            type: tmpLocation.type,
            subtype: 'a subtype',
            location: tmpLocation
      },
      encounter = new Encounter(encoutnerItem);
  encounter.save();
}
Careplan.prototype.save = function (callback) {
  id = this._id || Random.id();

  if (!this._carePathwayStatus) {
    var option = this.getTreatmentOption();
    if (option) {
      this.carePathwayStatus = option.carePathwayStatus;
    }
  } else {
    this.carePathwayStatus = this._carePathwayStatus;
  }

  this.pathway = this.pathway || [];
  if (this.carePathwayStatus && this.pathway.indexOf(this.carePathwayStatus) === -1) {
    this.pathway.push(this.carePathwayStatus);
  }

  var item = {
    _id: id,
    patientId: this.patientId,
    name: this.name,
    status: this.getStatus(this.status), //planned, active, onhold,completed,cancelled
    createdBy: this.createdBy,
    startDate: this.startDate,
    endDate: this.endDate,
    createdDate: this.createdDate || new Date(),
    presidingPhysician: this.presidingPhysician,
    updatedDate: new Date(),
    updatedBy: this.updatedBy,
    carePathwayStatus: this.carePathwayStatus || PS_POST_SURGERY,
    pathway: [PS_POST_SURGERY],
    treatmentOptionId: this.treatmentOptionId,
    dateOfSurgery: this.dateOfSurgery,
    type: this.type || 'default',
    isActive: this.isActive || true
      //condition: this.condition
      //referralRequest: id of the referral documernt if available
  }
  // this.saveEncounter();
  Careplans.upsert(id, item, function () {
    if (callback) {
      return callback.bind(this);
    }
  });
  return id;
};

Careplan.prototype.getClinicalCategory = function (upperClinical, lowerClinical) {
    var clinicalUpper;
    var clinicalLower;

    if (/C[0-9]/.test(upperClinical)) {
        clinicalUpper = 'Cervical';
    }
    if (/C[0-9]/.test(lowerClinical)) {
        clinicalLower = 'Cervical';
    }
    var clinicalCategory = new Array();
    if (clinicalUpper === 'Cervical') {
        clinicalCategory.push(clinicalUpper);
        if (clinicalLower !== 'Cervical') {
            clinicalCategory.push('Thoracic/Lumbar');
        }
    } else {
        clinicalCategory.push('Thoracic/Lumbar');
    }
    return {
        clinicalCategory: clinicalCategory
    };
};

Careplan.prototype.setCarePathwayStatus = function (ps_status) {
    let item = {};
    if (pathwayStatus[ps_status]) {
        item.carePathwayStatus = ps_status;

        // TODO: move next block to afterUpdate hook [
            if (this.pathway && _.isArray(this.pathway)) {
                this.pathway.push(ps_status);
            }
            else {
                this.pathway = [ps_status];
            }
            this.pathway = _.uniq(this.pathway);
            item.pathway = this.pathway;
        // ]

        Careplans.upsert(this._id, item);
    }
};

Careplan.prototype.getCarePathwayStatus = function () {
    return this.carePathwayStatus;
};

Careplan.prototype.eventCarePathpay = function (event) {
    return this.carePathwayStatus;
};

Careplan.prototype.getCarePathwayStatus = function () {
    return this.carePathwayStatus;
};

// TODO: next will be depricated
Careplan.prototype.setPathwayStatus = function (reason) {
  let code  = parseInt(reason, 10),
      caption = pathwayStatus[code];
  code = caption ? code : PS_UNDIAGNOSED;
  if (code == PS_CHANGEDIAG) {
    this.createNewCareplan();
    code = PS_UNDIAGNOSED;
  }
  return code;
};

Careplan.prototype.archiveCareplan = function (params) {
  var questionnaires, carePathwayStatus = this.setPathwayStatus(params.reason);
  if (carePathwayStatus) {
    let updateOptions = {
        isActive: false,
        notes: params.notes,
        status: CS_INACTIVE,
        carePathwayStatus: this.setPathwayStatus(params.reason),
        carePathwayStatusOld: this.carePathwayStatus
    };

    this.update(updateOptions);

    questionnaires = Questionnaires.find({careplanId: this._id}, {fields: {_id: 1}}).fetch();
    questionnaires.forEach(function(q){
      Questionnaires.update({_id: q._id}, {
        $set: {
          isActive: false
        }
      });
    });

    // updateOptions.notes = 'Depends on parent careplan ' + this.id  + ' with notes:' + updateOptions.notes;
    // _.each(Careplans.find({partOf: this._id}, {_id: 1, name: 1}).fetch(), function(c){
    //   Careplans.findOne({_id: c._id}).archiveCareplan(updateOptions);
    // });

  }
  else {
    return 'Pathway Status not defined';
  }
};

Careplan.prototype.unarchiveCareplan = function (params) {
  // update careplan
  if (this.partOf) {
    var parent = Careplans.findOne({_id: this.partOf});
    if (parent.isActive === false) {
      throw "Parent careplan is InActive. It must be unarchived before unarchive current careplan";
      return false;
    }
  }
  this.update({
    isActive: true,
    notes: params.notes,
    status: CS_PLANNED,
    carePathwayStatus: this.carePathwayStatusOld || CS_ACTIVE
  });
  // todo: update questionaries
};

Careplan.prototype.update = function (params) {

    Careplans.update({_id: this._id}, {
        $set: params
    });
};

Careplan.prototype.getDiagnosis = function () {
    return CareplanDiagnosis.findOne({
      careplanId: this._id
    });
};

Careplan.prototype.getDiagnosisDetails = function () {
  let treatmentOptions, diagnosis;
  if (diagnosis = this.getDiagnosis()) {
    diagnosis.createdBy = Meteor.users.findOne(diagnosis.createdBy).profile.name;
    diagnosis.createdDate = moment(diagnosis.createdDate).format("DD.MM.YY HH:mm");
    diagnosis.principalPathology = JSON.stringify(diagnosis.principalSubPathology).replace(/[{}"]/g, '').replace(/[:]/g, ': ');
    diagnosis.Neurologically_intact = (diagnosis.Neurologically_intact) ? 'Yes': 'No';
    if (treatmentOptions = TreatmentOptions.findOne(diagnosis.treatmentOption)) {
      diagnosis.treatmentOption = treatmentOptions.name;
    }
  }
  return diagnosis;
};

Careplan.prototype.principalSubPathology = function (ica) {
  var pKey, principalSubPathology = {},

      principalSubPathologies = [ // indexes from ICASchema.PP1
        {PPDiscHerniation: 0},
        {PPDegenerative: 1},
        {PPStenosisWithoutSpondylolisthesisOrScoliosis: 2},
        {PPSpondylolisthesis: 3},
        {PPDeformity: 4},
        {PPTraumaticFracture: 5},
        {PPPathFracture: 6},
        {PPTumourType: 7},
        {PPInflammationType: 8},
        {PPInfection: 9},
        {PPVascular: 10}
      ];
  pKey = _.pairs(_.find(principalSubPathologies, function(p) {
    return  _.pairs(p)[0][1] == ica.PP1;
  }))[0][0];

  principalSubPathology[pKey] = (ica.hasOwnProperty(pKey)) ? ica[pKey] : {};
  return principalSubPathology;
};


Careplan.prototype.updateDiagnosis = function(options) {
    let cd = CareplanDiagnosis.findOne({careplanId: this._id});
    options.careplanId = this._id;
    if (cd) {
        CareplanDiagnosis.update(cd._id, {
            $set: options
        });
    }
    else {
        CareplanDiagnosis.insert(options);
    }
};
