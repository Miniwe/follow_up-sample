TreatmentTypes = new Mongo.Collection("treatmentTypes", {
  transform: function (item) {
    return new TreatmentType(item);
  }
});
TreatmentType = function (doc) {
  _.extend(this, doc);
}
TreatmentType.prototype.save = function () {
  var id = this._id || Random.id();
  TreatmentTypes.upsert(id, {
    _id: id,
    name: this.name
  });

};
TreatmentType.prototype.getOptions = function () {
  return TreatmentOptions.find({
    treatmentTypeId: this._id
  }).fetch();
};
