PastMedicalHistories = new Mongo.Collection("past_med_history", {
    transform(item) {
        return new PastMedicalHistory(item);
    }
});
PastMedicalHistories.attachSchema(PastMedicalHistorySchema);

PastMedicalHistories.list = (list) => {
  return PastMedicalHistories.find(query, options);
}

PastMedicalHistory = function (doc) {
  _.extend(this, doc);
};

PastMedicalHistory.prototype.save = () => {
  const id = this._id || Random.id();
  PastMedicalHistories.upsert(id, {
    _id: id
  });
};
//this should be pasted on teh client side
AutoForm.hooks({
    pmhForm: {
    onSuccess: function (formType, result) {
        // Router.go('patient', {_id: this.docId})
        App.showSnackbar({
            message: (formType == 'insert') ? 'Record created' : 'Record updated',
            timeout: 2000
        });
        }
    }
});