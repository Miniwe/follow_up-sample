Reports = new Mongo.Collection("reports", {
  transform: function (item) {
    return new Report(item);
  }
});

Reports.attachSchema(ReportsSchema);
//Reports.attachCollectionRevisions();


Report = function (doc) {
  _.extend(this, doc);
}


Report.prototype.save = function () {
  var id = this._id || Random.id();
  Reports.upsert(id, {
    _id: id,
    name: this.name
  });
};