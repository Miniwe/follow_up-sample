ListsSchema = new SimpleSchema({
  name: {
    type: String,
    label: "Name",
    max: 50,
    autoform: {
      placeholder: "type..."
    }
  },
  description: {
    type: String,
    label: "Description",
    optional: true,
    autoform: {
      rows: 10,
      placeholder: "type..."
    }
  },
  createdDate: {
    type: Date,
    autoValue: insertedTimestampDate
  },
  createdBy: {
    type: String,
    optional: true,
    autoValue: function () {
      if(!this.isSet){
        return this.userId;
      }
    }
  },
  patientId: {
    type: String,
    label: "Subject",
    autoform: {
      options: function () {
        return Patients.find().map(function (c) {
          return {label: c.name, value: c._id};
        });
      },
      fullTextSearch: true,
      maxSelections: 1
    },
    optional: true
  },
  subscribers: {
    type: [String],
    minCount: 1,
    maxCount: 100,
    optional: true
  },
  assignedTo: {
    type: [String],
    label: "Assignees",
    minCount: 0,
    maxCount: 100,
    optional: true,
    autoform: {
      options: usersOptions,
      fullTextSearch: true
    }
  },
  priority: {
    type: String,
    label: "Priority",
    allowedValues: ["low", "medium", "high", "urgent"],
    defaultValue: "low",
    optional: true,
    autoform: {
      options: "allowed",
      capitalize: true
    }
  },
  status: {
    type: String,
    allowedValues: ["new", "not started", "in progress", "completed", "archived"],
    defaultValue: "new",
    optional: true
  },
  isActive: {
    type: Boolean,
    defaultValue: true,
    optional: true
  },
  readOnly: {
    type: Boolean,
    defaultValue: true,
    optional: true
  },
  privacy: {
    type: String,
    label: "Privacy",
    allowedValues: ["public", "private"],
    defaultValue: "public",
    optional: true,
    autoform: {
      options: "allowed",
      capitalize: true
    }
  },
  dueDate: {
    type: Date,
    optional: true,
    autoValue: function () {
      var moment2 = moment(this.value);
      var valueIsNotDate = !moment2.isValid();
      var valueIsNotSet = !this.isSet;
      if (valueIsNotSet || valueIsNotDate) {
        return new Date();
      }
    },
    autoform: {
      defaultValue: function () {
        return new Date();
      }
    }
  }
});

