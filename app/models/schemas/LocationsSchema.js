 LT_HOSPITAL = 'hospital';
 LT_CLINIC = 'clinic';

const DEFAULT_HOSPITAL = 'Montreal Neurological Institute';
const DEFAULT_CENTRE = 'ICU';

const LocationsSchemaTypes = [
    {value: LT_HOSPITAL, label: 'Hospital'},
    {value: LT_CLINIC, label: 'Clinic'}
];

FunctionalCentresSchema = new SimpleSchema({
    code: {
        label: 'Code',
        type: String
    },
    name: {
        type: String,
        label: 'Name'
    }
});

LocationsSchema = new SimpleSchema({
    type: {
        type: String,
        label: 'Type',
        autoform: {
            type: 'select-radio',
            options() {
                return LocationsSchemaTypes;
            }
        }
    },
    name: {
        type: String,
        optional: false,
        label: 'Name'
    },
    location_number: {
        type: String,
        optional: true,
        label: 'Number'
    },
    province: {
        type: String,
        optional: true,
        label: 'Province'
    },
    city: {
        type: String,
        optional: true,
        label: 'City'
    },
    functional_centres: {
        type: [FunctionalCentresSchema],
        label: 'Functional Centres',
        optional: true,
        blackbox: true
    }
});

LocationFormSchema = new SimpleSchema({
    type: {
        type: String,
        optional: false,
        label: 'Type',
        autoform: {
            type: 'select',
            options: LocationsSchemaTypes,
            value: LT_HOSPITAL,
        },
    },
    name: {
        type: String,
        optional: true,
        label: 'Name',
        autoform: {
            type: 'select',
            options() {
                const params = {
                    type: 'N/A'
                };
                if (Meteor.isClient) {
                    params.type = AutoForm.getFieldValue('location.type');
                    params.type = params.type || LT_HOSPITAL;
                }
                return Locations.find(params).fetch().map((location) => {
                    return {
                        value: location._id,
                        label: `${location.name} (${location.location_number})`
                    };
                });
            },
            value() {
                if (Meteor.isClient) {
                    const params = {
                        type: LT_HOSPITAL,
                        name: DEFAULT_HOSPITAL,
                    };
                    const location = Locations.findOne(params);
                    return location._id;
                }
                return '';
            },
        }
    },
    ft_code: {
        label: 'Functional Center',
        type: String,
        optional: true,
        autoform: {
            type: 'select',
            options() {
                let location,
                    fcs = [],
                    params = {};

                if (Meteor.isClient) {
                    params._id = AutoForm.getFieldValue('location.name');
                    location = Locations.findOne(params);
                    if (!location) {
                        location = Locations.findOne({ name: DEFAULT_HOSPITAL });
                    }
                    if (location) {
                      fcs = location.functional_centres.map(function(fc){
                          return {
                              value: fc.code,
                              label: fc.name
                          }
                      });
                    }
                }
                return fcs;
            },
            value() {
                if (Meteor.isClient) {
                    const params = {
                        type: LT_HOSPITAL,
                        name: DEFAULT_HOSPITAL,
                    };
                    const location = Locations.findOne(params);
                    if (location) {
                        const centre = location.functional_centres.find(fc => fc.name === DEFAULT_CENTRE);
                        return centre && centre.code;
                    }
                }
                return '';
            }
        }
    }
});
