let avatarColors = [
  'red', 'orange', 'yellow', 'olive',
  'green', 'teal', 'blue', 'violet',
  'purple', 'pink', 'brown', 'grey', 'black'
];

PatientAddressSchema = new SimpleSchema({
  address1: {
    type: String,
    label: "Address 1",
    max: 100,
    optional: true
  },
  address2: {
    type: String,
    label: "Address 2",
    max: 100,
    optional: true
  },
  mailingAddress: {
    type: String,
    optional: true
  },
  city: {
    type: String,
    label: "City",
    max: 50,
    optional: true
  },
  state: {
    type: String,
    label: "State",
    max: 30,
    optional: true
  },
  postalCode: {
    type: String,
    label: "Postal code",
    regEx: SimpleSchema.RegEx.ZipCode,
    optional: true
  },
  country: {
    type: String,
    label: "Country",
    defaultValue: "Canada",
    optional: true,
    autoform: {
      fullTextSearch: true,
      maxSelections: 1,
      options: function () {
        var countries = [];
        var countriesList = Countries();
        for (var i = 0; i < countriesList.length; i++) {
          countries.push({
            label: countriesList[i].name,
            value: countriesList[i].name
          })
        }
        return countries;
      }
    }
  },
  use: {
    type: String,
    label: "Use",
    allowedValues: ['Home', 'Work', 'Temp'],
    autoform: {
      options: "allowed"
    },
    optional: true
  },
  type: {
    type: String,
    label: "Type",
    allowedValues: ["Home", "Work"],
    defaultValue: "Home",
    autoform: {
      options: "allowed"
    },
    optional: true
  },
  email: {
    type: [String],
    label: "Email",
    regEx: SimpleSchema.RegEx.Email,
    optional: true
  },
  home: {
    type: String,
    label: "Home",
    optional: true
  },
  cell: {
    type: String,
    label: "Cell",
    optional: true
  },
  workPhone: {
    type: String,
    label: "Work phone",
    optional: true,
    autoform: {
      mask: "(999) 999-9999",
      placeholder: "(999) 999-9999"
    }
  }
});


PatientInfoSchema = new SimpleSchema({
  firstName: {
    type: String,
    label: "First Name",
    optional: true,
    autoform: {
      placeholder: "First Name"
    }
  },
  midName: {
    type: String,
    label: "Middle Name",
    optional: true,
    autoform: {
      placeholder: "Middle Name"
    }
  },
  lastName: {
    type: String,
    label: "Last Name",
    optional: true,
    autoform: {
      placeholder: "Last Name"
    }
  },
  name: {
    type: String,
    label: "Name",
    optional: true,
    autoform: {
      placeholder: "Name"
    },
    autoValue: function() {
      return this.field('firstName').value + ' ' + this.field('lastName').value;
    }
  },
  email: {
    type: String,
    label: "Email",
    regEx: SimpleSchema.RegEx.Email,
    optional: true
  },
  userId: {
    type: SimpleSchema.RegEx.Id,
    optional: true,
    defaultValue: function () {
        return this.userId;
    }
  },
  IdType: {
    type: String,
    optional: true,
    defaultValue: "",
  },
  generatedNumber: {
    type: Number,
    optional: true,
  },
  verified: {
    type: String,
    optional: true,
    label: "e-mail verified"
  },
  verificationDate: {
    type: Date,
    autoValue: insertedTimestampDate
  },
  birthDate: {
    type: Date,
    label: "Birth date",
    max: function () {
      return new Date();
    },
    autoform: {
      defaultValue: function () {
        return new Date();
      }
    },
    optional: true
  },

  gender: {
    type: String,
    label: "Gender",
    defaultValue: "Male",
    allowedValues: ["Other", "Male", "Female"],
    autoform: {
      options: [
        // {label: "Not specified", value: "notSpecified"},
        {label: "Male", value: "Male"},
        {label: "Female", value: "Female"},
        {label: "Other", value: "Other"}
      ]
    }
  },
  phone: {
    type: String,
    label: "Phone",
    autoform: {
      type: "masked-input",
      mask: "(999) 999-9999",
      placeholder: "(999) 999-9999"
    },
    optional: true
  },
  csorn: {
    type: String,
    optional: true,
    label: "CSORN"
  },
  phn: {
    type: Object,
    label: "Patient identifier",
    optional: true
  },
  "phn.type": {
    type: String,
    label: "ID Type",
    optional: true,
    defaultValue: "RAMQ",
    allowedValues: ["Other", "RAMQ", "MRN"],
    autoform: {
      options: [
        // {label: "Not specified", value: "notSpecified"},
        {label: "MRN", value: "MRN"},
        {label: "RAMQ", value: "RAMQ"},
        {label: "Other", value: "Other"}
      ]
    }
  },
  "phn.value": {
    type: String,
    optional: true,
    label: "MRN/Health card"
  },

//  If phn.type==='RAMQ'
//  then we can use the following mask: "ZZZZ-YYMMDD##, else blank (for now)
// see w/f for rules.  Note, we need to check if the patient is if the value is
// elderly or a child

  isActive: {
    type: Boolean,
    optional: true,
    defaultValue: true
  },
  enrolmentDate: {
    type: Date,
    autoValue: insertedTimestampDate
  },
  createdDate: {
    type: Date,
    autoValue: insertedTimestampDate
  },
  createdBy: {
    type: String,
    autoValue: function () {
      if(!this.isSet) {
        return this.userId;
      }
    },
    optional: true
  },
  assignedTo: {
    type: String,
    label: "Assigned to",
    autoform: {
      options: usersOptions,
      defaultValue: function () {
        return Meteor.userId();
      },
      fullTextSearch: true,
      maxSelections: 1
    },
    optional: true
  },
  presidingPhysician: {
    type: String,
    label: "Presiding Physician",
    autoform: {
      options: usersOptions,
      defaultValue: function () {
        return Meteor.userId();
      },
      fullTextSearch: true,
      maxSelections: 1
    },
    optional: true
  },
  status: {
    type: String,
    label: "Status",
    defaultValue: "active"
  },
  address: {
    type: PatientAddressSchema
  },
  carePathwayStatus: {
    type: String,
    optional: true
  },
  avatarColor: {
    type: String,
    optional: true,
    label: 'Avatar Color',
    autoValue: function() {
      if (this.isInsert) {
        return _.sample(avatarColors);
      }
    }
  },
  avatar: {
    type: String,
    optional: true
  },
  prefix: {
    type: String,
    optional: true
  },
  suffix: {
    type: String,
    optional: true
  },
  maritalStatus: {
    type: String,
    optional: true
  },
  idType: {
    type: String,
    optional: true
  },
});
