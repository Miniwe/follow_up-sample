import { LocationsSchema } from './LocationsSchema.js';

let getPatientFromForm = () => {
  return AutoForm.getFieldValue('patientId', 'insertAdmissionForm') || AutoForm.getFieldValue('patientId', 'updateAdmissionForm');
};

AdmissionsHistorySchema = new SimpleSchema({
    updatedAt: {
      label: 'Updated At',
      type: Date
    },
    updatedBy: {
      label: 'Updated By',
      type: String,
      regEx: SimpleSchema.RegEx.Id
    },
    checkedOut: {
      label: 'Checked Out',
      type: Date
    }
});

AdmissionsSchema = new SimpleSchema({
  _id: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    patientId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
      label: 'Patient',
      autoform: {
        options: function () {
          let formId = AutoForm.getFormId();
          let admissions, options;

          if (formId != 'ReportForm') {
              options = {
                  checkedIn: {$ne: null},
                  checkedOut:  null
              };
          }

          admissions = Admissions.find(options);

          let patientsIds = []
          admissions.forEach( function ( admission ) {
            patientsIds.push(admission.patientId)
          });

          let patients = [];
          Patients.find({ _id: { $nin: patientsIds} }).map(function (c) {
            patients.push({label: c.name, value: c._id});
          })

          if (formId != 'ReportForm') {
              patients.push({label: '+ Create new patient', value: 'new'});
          }

          return patients;
        },
        fullTextSearch: true
      },
      custom() {
        let admission = Admissions.findOne({
          patientId: this.value,
          checkedIn: {$ne: null},
          checkedOut: null
        });
        if (this.isInsert && admission) {
          return 'checkedIn';
        }
        if (this.isUpdate && admission && !this.field('checkedOut').value) {
          return 'checkedIn';
        }

      }
    },
    careplanId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
      label: 'Careplan',

      optional: true,
      autoform: {
        type: 'hidden',
        options: function () {
          let patientId,
            options  = {
              isActive: true
          };
          if (patientId = getPatientFromForm()) {
            options.patientId = patientId;
          }
          return Careplans.find(options).map(function (c) {
            return {label: c.name, value: c._id};
          });
        },
        fullTextSearch: true
      }
    },
    checkedIn: {
      type: Date,
      label: 'Date/Time of admission',
      autoform: {
        type: 'datetime-local',
        defaultValue: function () {
          return moment().format("YYYY-MM-DDTHH:mm");
        }
      }
    },
    checkedOut: {
      type: Date,
      label: 'Discharge date/time',
      autoform: {
        type: 'datetime-local',
      },
      optional: true
    },
    location: {
        type: LocationFormSchema,
        label: 'Location',
        optional: false
    }
});
