CareplanDiagnosisSchema = new SimpleSchema({
    careplanId: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        label: 'Careplan Id'
    },
    referringDocumentation: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        label: 'Referring Document',
        optional: true
    },
    createdBy: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
        autoValue: function() {
            return this.userId;
        },
        label: 'Created By'
    },
    createdDate: {
        type: Date,
        autoValue: function() {
            return new Date();
        },
        label: 'Created Date'
    },
    reason_for_admission: {
        type: Object,
        label: 'Reason for Admission - cd',
        optional: true, // @todo: must be required
        blackbox: true
    },
    admission_date: {
        type: Date,
        label: 'Admission Date',
        optional: true
    },
    discharge_date: {
        type: Date,
        label: 'Discharge Date',
        optional: true
    },
    thromboprophylaxis: {
        type: Number,
        label: 'Thromboprophylaxis',
        optional: true,
        defaultValue: 0
    },
    length_of_stay: {
        type: Number,
        label: 'Length of Stay',
        optional: true
    }
});