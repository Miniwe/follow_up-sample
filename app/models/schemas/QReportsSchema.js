var conditions = function (schema, fields) {
    return [
        {value: '$eq',   label: ' = '},
        {value: '$ne',   label: ' != '},
        {value: '$gt',   label: ' > '},
        {value: '$lt',   label: ' < '},
        {value: '$gte',  label: ' >= '},
        {value: '$lte',  label: ' <= '},
        {value: '$text', label: 'text'}
    ];
};

var getFields = function (schema, fields) {
    let list = [];
    fields.forEach(function(field){
        if (_.indexOf(field, schema._schemaKeys)) {
            list.push({
                value: field,
                label: Modules.both.getLabel(schema, field)
            });
        }
    });
    return list;
};

AdmissionСriterionSchema = new SimpleSchema({
    field:  {
        type: String,
        label: 'Field',
        autoform: {
          options: function () {
            return getFields(AdmissionsSchema, [
                "patientId","checkedIn","checkedOut",
                "location.name","location.type"
            ]);
          }
        }
    },
    condition: {
        type: String,
        label: 'Condition',
        autoform: {
          options: function () {
            return conditions();
          },
          defaultValue: '$eq'
        }
    },
    value:  {
        type: String,
        label: 'Value'
    },
    type:  {
        type: String,
        label: 'Type'
    }
});

FormСriterionSchema = new SimpleSchema({
    field:  {
        type: String,
        label: 'Field',
        autoform: {
          options: function () {
            var fields = [],
                noteName = (this.name.match(/formsCriteriа.\d+/)) + '.form',
                noteField = AutoForm.getFieldValue(noteName);

            switch (noteField) {
                case 'AdmissionNote':
                    fields = getFields(AdmissionNoteSchema, [
                        "reason_for_admission.$.name", "medical_history.$.name", "allergies.$.name",
                        "habits.tobacco.frequency", "other_details", "surgical_history.$.year", "habits.tobacco"
                    ]);
                    break;
                case 'ConsultNote':
                    fields = getFields(CNchema, CNchema._schemaKeys);
                    break;
                case 'DischargeNote':
                    fields = getFields(DischargeNoteSchema, [
                        "admission_date", "discharge_date", "length_of_stay", "Death.type",
                        "reason_for_admission.$.name", "dischargeSummary"
                    ]);
                    break;
            }
            return fields;
          }
        }
    },
    condition: {
        type: String,
        label: 'Condition',
        autoform: {
          options: function () {
            return conditions();
          }
        }
    },
    value:  {
        type: String,
        label: 'Value'
    },
    type:  {
        type: String,
        label: 'Type'
    }
});

AdmissionFormSchema = new SimpleSchema({
    form:  {
        type: String,
        label: 'Form',
        autoform: {
          options: function () {
            return [
                {value: 'AdmissionNote', label: 'Admission Note'},
                {value: 'ConsultNote', label: 'Consult Note'},
                {value: 'DischargeNote', label: 'Discharge Note'}
            ];
          }
        }
    },
    formСriteria: {
        type: [FormСriterionSchema],
        label: 'Form Criteria',
        minCount: 0,
        optional: true,
        autoform: {
            initialCount: 0
        }
    }

});

ReportsSchema = new SimpleSchema({
    _id: {
        type: String,
        regEx: SimpleSchema.RegEx.Id,
    },
    name: {
        type: String,
        label: 'Name',
        optional: false
    },
    template: {
        type: String,
        label: 'Group by',
        optional: false,
        autoform: {
            type: "select-radio-inline",
            // type: "semanticUI",
            options: function() {
                return [
                    // {value: 'report_admissions_forms', label: 'Admissions + Note Forms'},
                    // {value: 'report_admissions_only', label: 'Admissions'},
                    // {value: 'report_forms_only', label: 'Note Forms'},
                    {value: 'results_table', label: 'Group by form type'},
                    {value: 'grouped_results_table', label: 'Group by careplan'}
                ];
            }
        },
        defaultValue: 'admissions_forms'
    },
    userId: {
        type: String,
        optional: false,
        autoform: {
            type: 'hidden'
        },
        autoValue: function () {
            return Meteor.userId();
        }
    },
    admissionsCriteriа: {
        type: [AdmissionСriterionSchema],
        label: 'Criteria',
        minCount: 0,
        optional: true,
        autoform: {
            initialCount: 0
        }
    },
    formsCriteriа: {
        type: [AdmissionFormSchema],
        label: 'Form Criteria',
        minCount: 0,
        optional: true,
        autoform: {
            initialCount: 0
        }
    }
});
