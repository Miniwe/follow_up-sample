EncounterSchema = new SimpleSchema({
  admissionId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    label: "Admission",
    optional: false,
    autoValue: function() {
      let patientId = this.field('patientId').value,
          admission = Admissions.findOne({
              patientId: patientId,
              checkedIn: {$ne: null},
              checkedOut: null
          });
      return admission._id;
    }
  },
  patientId: {
    type: String,
    label: "Patient",
    optional: false,
    autoform: {
      options: function () {
        return Patients.find({}).map(function (c) {
          return {label: c.name, value: c._id};
        });
      },
      fullTextSearch: true
    }
  },
  practitioner: {
    type: Object,
    optional: true
  },
  "practitioner.Id": {
    type: String,
    label: "Practitioner ID",
    optional: true
  },
  "practitioner.name": {
    type: String,
    label: "Practitioner name",
    optional: true
  },
  location: {
      type: LocationFormSchema,
      label: 'Location',
      optional: false
  },
  type: {
    type: String,
    label: "Encounter Type",
    optional: true,
            autoform: {
      type: "select-radio",
      //class: 'select multiple',
      options: function () {
        return [{
          label: "Initial Visit / Visite principale",
          value: "Initial visit"
        }, {
          label: "Follow-up Visit / Visite de controle",
          value: "FollowUp Consult"
        }, {
          label: "Discharge",
          value: "Discharge"
        }];
      }
    }
  },
  // subtype: {
  //   type: String,
  //   label: "Subtype",
  //   optional: true,
  //   autoValue: function() {
  //     return  'Consult Note';
  //   }
    // autoform: {
    //   options: function () {
    //     if (Meteor.isClient) {
    //       const location = AutoForm.getFieldValue('location.type');
    //       const type = AutoForm.getFieldValue('type');
    //       if (location && type) {
    //         var types = new EncounterType(location);
    //         var subtypes = types.getSubtypes(type);

    //         if (subtypes && subtypes.length) {
    //           var subTypesMap = types.getSubtypes(type).map(function (item) {
    //             return {
    //               label: item,
    //               value: item
    //             }
    //           });
    //           // console.log(subTypesMap, location, type);
    //           return subTypesMap;
    //         }
    //       }
    //     }
    //   }
    // }
  // },
  start: {
    type: Date,
    label: 'Start time',
    optional: false,
    autoform: {
      type: 'datetime-local',
      defaultValue: function () {
        return moment().format("YYYY-MM-DDTHH:mm");
      }
    }
  },
  end: {
    type: Date,
    label: 'End time',
    optional: false,
    custom: function () {
      // get a reference to the fields
      var start = this.field('start');
      var end = this;
      // Make sure the fields are set so that .value is not undefined
      if (start.isSet && end.isSet) {
        if (moment(end.value).isBefore(start.value)) return 'badDate';
      }
    },
    autoform: {
      type: 'datetime-local',
      defaultValue: function () {
          //console.log('default date');
          var startDate = AutoForm.getFieldValue('start', 'encounter');
          return startDate ? moment(startDate).add(15, 'm').format("YYYY-MM-DDTHH:mm") : '';
      }
    }
  },
  dateOfReferral: {
    type: Date,
    optional: true,
    label: "Date of Referral"
  },
  dateOfAdmission: {
    type: Date,
    optional: true,
    label: "Date of Admission"
  },
  dateOfDischarge: {
    type: Date,
    optional: true,
    label: "Date of Discharge"
  },
  partOf: {
    type: String,
    optional: true,
    label: "Part of"
  },
  patientName: {
    type: String,
    optional: true,
    autoValue: function () {
      if (Meteor.isTest || Meteor.isAppTest) {
        return 'Patient Username 1';
      }
      else {
          let patientId = this.field('patientId').value;
          return Patients.findOne({_id: patientId}).name;
      }
    }
  },
  patientPhn: {
    type: String,
    optional: true,
    autoValue: function () {
      if (Meteor.isTest || Meteor.isAppTest) {
        return 'Username 2';
      }
      else {
          let patient = Patients.findOne({_id: this.field('patientPhn').value});
          //return Meteor.users.findOne({_id: patient.presidingPhysician}).profile.name || '';
         // return Patients.findOne({_id: patientId}).phn;
      }
    }
  },
  participants: {
    type: [String],
    optional: true,
    autoValue: function () {
      return [Meteor.userId()]
    }
  },
  participantName: {
    type: String,
    optional: true,
    autoValue: function () {
      if (Meteor.isTest || Meteor.isAppTest) {
        return 'Username 1';
      }
      else {
            return Meteor.users.findOne({_id: Meteor.userId()}).profile.name || '';
      }
    }
  },
  participantId: {
    type: String,
    optional: true,
    autoValue: function () {
      return Meteor.userId();
    }
  },
  questionnaireId: {
    type: String,
    optional: true
  },
  careplanId: {
    type: String,
    label: 'Careplan',
    optional: true,
    autoValue() {
        return Admissions.findOne({_id: this.field('admissionId').value}).careplanId;
    }
    // autoform: {
    //   options: function () {
    //     if (Meteor.isClient) {
    //       var subject = AutoForm.getFieldValue('patientId');
    //       return Careplans.find({patientId: subject}).map(function (c) {
    //         return {label: c._id, value: c._id};
    //       });
    //     }
    //   }
    // }
  },
  participantNumber: {
    type: String,
    optional: true,
    defaultValue: ''
  }
});


SimpleSchema.messages({
  badDate: 'End date must be after the start date.',
  notDateCombinationUnique: 'The start/end date combination must be unique'
});
