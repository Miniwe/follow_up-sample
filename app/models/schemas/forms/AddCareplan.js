AddCareplan = new SimpleSchema({
  name: {
    type: String,
    optional: false,
    label: 'Name'
  },
  presidingPhysician: {
    type: String,
    label: "Physician",
    autoform: {
      defaultValue: function() {
        return Meteor.userId();
      },
      options: function () {
        return Meteor.users.find().map(function (c) {
          return {label: c.profile.name, value: c._id};
        });
      },
      maxSelections: 1
    },
    optional: false

  },
  related: {
    type: String,
    optional: true,
    label: 'Related careplan',
    autoform: {
      options: function () {
        var options = {};
        // if (cid) { // TODO: add patient options
        //     options.patientId = Careplans.findOne(cid).patientId;
        // }
        return Careplans.find(options).map(function (c) {
          return {label: c.name, value: c._id};
        });
      },
      maxSelections: 1
    }
  }
});