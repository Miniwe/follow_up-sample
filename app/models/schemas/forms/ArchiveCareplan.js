ArchiveCareplan = new SimpleSchema({
  reason: {
    type: String,
    optional: false,
    label: 'Reason',
    autoform: {
      type: 'select',
      placeholder: 'Select Reason',
      options: function () {
          return _.map([PS_WITHDREW, PS_UNABLE_TO_CONTINUE, PS_COMPLETED_TREATMENT,
                        PS_TRANSFERRED, PS_DISCHARGE_NO_FURTHER_TREATMENT_REQUIRED,
                        PS_CHANGEDIAG], function(code, key){
                  return {
                      value: code, label: pathwayStatus[code]
                  };
              });
        }
    }
  },
  notes: {
    type: String,
    optional: true,
    label: 'Additional Notes',
    autoform: {
      afFieldInput: {
        placeholder: 'Additional Notes',
        type: 'textarea'
      }
    }
  }
});