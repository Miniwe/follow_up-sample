CRA_CREATENEWBASED = 1;
CRA_SETPARENT = 2;

ModifyCareplan = new SimpleSchema({
  name: {
    type: String,
    optional: false,
    label: 'Name'
  },
  presidingPhysician: {
    type: String,
    label: "Physician",
    autoform: {
      options: function () {
        return Meteor.users.find().map(function (c) {
          return {label: c.profile.name, value: c._id};
        });
      },
      fullTextSearch: true,
      maxSelections: 1
    },
    optional: false

  },
  related: {
    type: String,
    optional: true,
    label: 'Related careplan',
    autoform: {
      options: function () {
        var options = {};
        var cid = Session.get('modifyCareplanId');
        options._id = {$ne: cid};
        if (cid) {
            options.patientId = Careplans.findOne(cid).patientId;
        }
        return Careplans.find(options).map(function (c) {
          return {label: c.name, value: c._id};
        });
      },
      fullTextSearch: true,
      maxSelections: 1
    }
  },
  action: {
    type: String,
    optional: true,
    label: 'Actions for Related',
    autoform: {
      type: "select-radio",
      defaultValue: CRA_SETPARENT,
      options: function () {
        if (AutoForm.getFieldValue('related','modifyCareplan')) {
          return [
            // {value: CRA_CREATENEWBASED, label: 'Create Careplan based on Related'},
            {value: CRA_SETPARENT, label: 'Set Related as Parent'}
          ];
        }
      }
    }
  }
});