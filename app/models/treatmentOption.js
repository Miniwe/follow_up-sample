TreatmentOptions = new Mongo.Collection("treatmentOptions", {
  transform: function (item) {
    return new TreatmentOption(item);
  }
});
TreatmentOption = function (doc) {
  _.extend(this, doc);
}
TreatmentOption.prototype.getForms = function () {
  return Forms.find({
    _id: {
      $in: this.forms
    }
  }).fetch() || [];
}
TreatmentOption.prototype.save = function () {
  var id = this._id || Random.id();
  TreatmentOptions.upsert(id, {
    _id: id,
    name: this.name,
    treatmentTypeId: this.treatmentTypeId,
    forms: this.forms || [],
    carePathwayStatus: this.carePathwayStatus
  });
};
TreatmentOption.prototype.getType = function () {
  return TreatmentTypes.findOne({
    _id: this.treatmentTypeId
  });
};
