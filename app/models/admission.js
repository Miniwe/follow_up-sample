import { _ } from 'underscore';

Admissions = new Mongo.Collection("admissions", {
    transform(item) {
        return new Admission(item);
    }
});


Admissions.attachSchema(AdmissionsSchema);

//Admissions.attachCollectionRevisions();

Admissions.list = (list) => {
    let query = {},
    options = {};

    switch (list) {
        case 'checked-in':
            query = {
                checkedOut: null
            };
            options.sort = {
                checkedIn: -1
            }
        break;
        case 'checked-out':
            query = {
                checkedOut: {$ne: null}
            };
            options.sort = {
                checkedOut: -1
            }
            break;
    }
    return Admissions.find(query, options);
};


let makeQuery = function (conditions) {
    let query = {};
    _.each(conditions, (c) => {
        // console.log('test condition', c);
        let o = {};
        switch (c.condition) {
            case '$text':
                o = new RegExp('.*'+ c.value +'.*', 'i');
                break;
            default:
                switch (c.type) {
                    case 'Date':
                        o[c.condition] = moment(new Date(c.value)).toDate();
                        break;
                    case 'Number':
                        o[c.condition] = parseFloat(c.value);
                        break;
                    default:
                        o[c.condition] = c.value;
                }
        }

        if (_.find(query, (v, k) => (k == c.field) )) {
            query[c.field] = _.extend(query[c.field], o);
        }
        else {
            _.extend(query,  _.reduceRight(c.field.split('$'), function(obj, item){
                let a = {}, tItem = item;

                if (item[0] == '.') {
                    tItem = item.substr(1,item.length);
                }

                if (item[item.length-1] == '.') {
                    tItem =  tItem.substr(tItem,tItem.length - 1);
                    a[tItem] = {$elemMatch: obj};
                }
                else {
                    a[tItem] = obj;
                }
                return a;
            }, o));
        }

    });
    return query;
};


Admissions.queryResults = (options, params) => {
    let query = {},
        reportId = options.reportId,
        report,
        admissionsIds = [];

    params = params || {};

    if (!reportId) {
        return Admissions.find(options, params);
    }

    // get reportId
    report = Reports.findOne(reportId);

    // if isset forms conditions
    if (report.formsCriteriа && report.formsCriteriа.length) {
        let questionnairesIds = [];

        report.formsCriteriа.forEach((noteForm) => {
            let collection, formQuestionnairesIds,
                formQuery;

            if (noteForm) {
                formQuery = makeQuery(noteForm.formСriteria);

                switch (noteForm.form) {
                    case 'AdmissionNote':
                        collection = AdmissionNotes;
                        break;
                    case 'ConsultNote':
                        collection = ConsultNotes;
                        break;
                    case 'DischargeNote':
                        collection = DischargeNotes;
                        break;
                }

                // console.log(`${noteForm.form} query`, formQuery);
                formQuestionnairesIds = _.map(collection.find(formQuery, {fields: {questionnaireId: 1}}).fetch(), (item) => {
                    return item.questionnaireId;
                });

                _.extend(questionnairesIds, formQuestionnairesIds);
            }
        });

        admissionsIds = _.map(Questionnaires.find({_id: {$in: questionnairesIds}},{fields: {encounterId: 1}}).fetch(), (item) => {
            return Admissions.findOne(Encounters.findOne(item.encounterId).admissionId)._id;
        });

        query._id = {$in: admissionsIds};
    }

    if (report.admissionsCriteriа && report.admissionsCriteriа.length) {
        _.extend(query, makeQuery(report.admissionsCriteriа));
    }

    // console.log(`Admissions query`, JSON.stringify( query ));
    admissions = Admissions.find(query, params);
    return admissions;
};


Admissions.after.update((userId, doc, fieldNames, modifier, options) => {
    if (doc.checkedIn && doc.checkedOut) {

        if (Meteor.isClient) {
            Router.go('admission', {_id: doc._id});
        }

        if (Meteor.isServer) {
            Careplans.findOne({_id: doc.careplanId}).archiveCareplan({
                reason: PS_ADMISSION_DISCHARGED,
                notes: `New Admission ${doc._id} Created`
            });
        }
    }
});


Admissions.after.insert((userId, doc) => {
    if (Meteor.isServer) {
        let careplanId = (doc.careplanId) ? doc.careplanId : doc.patientId;
        Careplans.findOne({_id: careplanId}).archiveCareplan({
            reason: PS_SUBCAREPLAN,
            notes: `New Admission ${doc._id} Created`
        });
        // create subcareplan

        let newCareplan = Patients.findOne(doc.patientId).addCareplan(false, `Careplan for Admission ${doc._id}`);
        newCareplan.update({
            patientId: doc.patientId,
            partOf: careplanId,
            pathway: [PS_POST_SURGERY],
            carePathwayStatus: PS_POST_SURGERY,
            type: 'subcareplan'
        });
        Admissions.update({
            _id: doc._id
        }, {
            $set: {
                careplanId: newCareplan._id
            }
        });

    }
});


Admission = function (doc) {
    _.extend(this, doc);
};


Admission.prototype.save = function() {
    const id = this._id || Random.id();
    Admissions.upsert(id, {
        _id: id
    });
};


Admission.prototype.getLocationName = function() {
    let location = Locations.findOne(this.location.name),
        lName = 'N/A', ftName = '';

    if (location) {
        lName = (_.map(location.name.split(" "), (item) => item.substr(0,1).toUpperCase() )).join('');
        if (location.functional_centres && this.location.ft_code) {
            ftName = _.find(location.functional_centres, (item) => (item.code == this.location.ft_code)).name;
        }
    }

    return `${lName}-${ftName}`;
};


Admission.prototype.checkout = function () {
    Admissions.update({
        _id: this._id
    }, {
        $set: {
            checkedOut: new Date()
        }
    });
};

Admission.prototype.getForms = function (report) {
    let forms = [],
        careplanId = this.careplanId,
        aOs;

    // console.log('in get forms result', forms);
    if (!(report instanceof Report))  {
        if (Meteor.isClient && Session.get('current_report')) {
            report = Reports.findOne(Session.get('current_report'));
        }
        else {
            return forms;
        }
    }

    if (report && report.formsCriteriа) {
        aOs = _.map(Questionnaires.find({careplanId: careplanId}).fetch(), function(item){
            return item._id;
        });
        report.formsCriteriа.forEach((noteForm) => {
            let collection, criteriaForms,
                formQuery;

            if (noteForm) {
                formQuery = makeQuery(noteForm.formСriteria);

                switch (noteForm.form) {
                    case 'AdmissionNote':
                        collection = AdmissionNotes;
                        break;
                    case 'ConsultNote':
                        collection = ConsultNotes;
                        break;
                    case 'DischargeNote':
                        collection = DischargeNotes;
                        break;
                }
                formQuery._id = {$in: aOs};
                // console.log('in get forms', noteForm.form, JSON.stringify(formQuery));
                criteriaForms = collection.find(formQuery);
                forms = _.union(forms, criteriaForms.fetch());
            }
        });
    }
    return forms;
};
