let generate_print = function(encounterId) {

    let html = [ ],
        encounter = Encounters.findOne(encounterId),
        form, pdfTemplate, document_id,
        questionnaire = Questionnaires.findOne({encounterId: encounterId}),
        admission = Admissions.findOne(encounter.admissionId),
        patient = Patients.findOne({_id: questionnaire.patientId}),
        formName = questionnaire.name,
        filenameAr = [
            formName.replace(/[^a-z]/gi, '_'),
            patient.name.replace(/[^a-z]/gi, '_'),
            moment().format('YYYYMMDD')
        ],
        filename = `${filenameAr.join('__')}.pdf`,
        orientation = "portrait";

    switch (questionnaire.type) {
        case "AdmissionNote":
            form = AdmissionNotes.findOne(questionnaire._id);
            pdfTemplate = Template.AdmissionNotePDF;
            document_id = "FMU-0656";
        break;
        case "ConsultNote":
            form = ConsultNotes.findOne(questionnaire._id);
            pdfTemplate = Template.consultNotePDF;
            document_id = "FMU-0339";
            orientation = "landscape";
        break;
        case "DischargeNote":
            form = DischargeNotes.findOne(questionnaire._id);
            pdfTemplate = Template.dischargeNotePDF;
            document_id = "FMU-0385";
        break;
    }


    let cssString = Assets.getText('print.css');

    html.push(Blaze.toHTMLWithData(pdfTemplate, {
        formName: formName,
        location: admission.getLocationName(),
        admission_date: moment(new Date(admission.checkedIn)).format('MM/DD/YYYY'),
        discharge_date: admission.checkedOut || 'N/A',
        los: moment(new Date(admission.checkedIn)).fromNow(true),
        phn: patient.phn,
        patientName: patient.name,
        form: form,
        document_id: document_id,
        server_url: 'https://mni.followupapp.ca'
    }));

    html.push(Blaze.toHTMLWithData(Template.pdfFooter, {
        prepared: 'Meteor.user().profile.name',
        date: moment(new Date()).format('YYYYMMDD'),
        status: questionnaire.status,
        document_id:  document_id
    }));

    let options = {
        "paperSize": {
            "format": "A4",
            "orientation": orientation,
            "margin": "0.5in"
        },
        phantomPath: Meteor.settings.path.phantomjs || "",
        siteType: 'html'
    };

    SSR.compileTemplate('pdfPrintLayout', Assets.getText('pdfPrintLayout.html'));

    Template.pdfPrintLayout.helpers({
          doctype() {
            return "<!DOCTYPE html>";
        },
        styles() {
            return cssString;
        },
        body(){
            return html.join('');
        }
    });

    let htmlLayout = SSR.render('pdfPrintLayout',{});

    return htmlLayout;
};

Modules.server.generate_print = generate_print;
