import { json2xml } from "json2xml";
import { JSZip } from "jszip";

let exportData = ( params, compileCallback ) => {
  let archive = _initializeZipArchive();
  compileCallback = compileCallback || _compileZip;
  compileCallback( archive, params );
  return _generateZipArchive( archive );
};

let _initializeZipArchive = () => {
  return new jsZip();
};

let _getDataFromCollection = ( collection, params ) => {
  let data = collection.find(params).fetch();
  if ( data ) {
    return data;
  }
};

let _generateZipArchive = ( archive ) => {
  return archive.generate( { type: 'base64' } );
};

let _formatData = {
  csv( data )  { return Papa.unparse( data ); },
  // xml( data )  { return json2xml( { 'posts': data }, { header: true } ); },
  // html( data ) {
  //   let header = Assets.getText( 'export/header.html' ),
  //       footer = Assets.getText( 'export/footer.html' );
  //   return header + data + footer;
  // },
  json( data ) { return JSON.stringify( data, null, 2 ); }
};

let _addFileToZipArchive  = ( archive, name, contents ) => {
  archive.file( name, contents );
};

let _parseQuestionnaires  = ( data ) => {
  return _.map(data, function(item) {
    item.patient = Patients.findOne(item.patientId).name;
    if (item.status == 'completed') {
        item.state = 'Completed By: ' + Meteor.users.findOne(item.completedBy).profile.name;
    }
    else if (item.dueDate) {
        item.state = 'Completed Within: ' + moment(item.dueDate).fromNow();
    }
    else {
        item.state = '';
    }
    item.dueDate = moment(item.dueDate).format("DD.MM.YY HH:mm");

    return item;
  });
};

let _prepareDataForArchive = ( archive, collection, options, type, fileName ) => {
  let formattedData,
      data = collection instanceof Mongo.Collection ? _getDataFromCollection( collection, options.params ) : collection;
  if (collection._name == 'questionnaires') {
      data = _parseQuestionnaires(data);
  }
  formattedData = _formatData[ type ] ( {
        data: data,
        fields: options.fields
  } );
  _addFileToZipArchive( archive, fileName, formattedData );
};

let _compileZip = ( archive, params ) => {
  let qFields =  ['_id', 'patientId', 'patient', 'name', 'type', 'dueDate', 'status', 'state'];
  let parsedParams = {};
  _.each(params, function(param, key) {
    switch (key ) {
        case "filterString":
            // parsedParams['$text'] = {'$search':  param}; // TODO: now $text is error
            break;
        default:
    }
  });
  _prepareDataForArchive( archive, Questionnaires, {fields: qFields, params: parsedParams}, 'csv', 'questionnaires.csv' );
};

Modules.server.exportData = exportData;
