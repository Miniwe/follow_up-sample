let ClearData = function (options) {
        options = options || {};
        var excludedCollections = ['system.indexes', '__kdconfig', '__kdtimeevents', '__kdtraces'];
        if (options.excludedCollections) {
            excludedCollections = excludedCollections.concat(options.excludedCollections);
        }
        var db = MongoInternals.defaultRemoteCollectionDriver().mongo.db;
        var getCollections = Meteor.wrapAsync(db.collections, db);
        var collections = getCollections();
        var appCollections = _.reject(collections, function (col) {
            return col.collectionName.indexOf('velocity') === 0 ||
            excludedCollections.indexOf(col.collectionName) !== -1;
        });

        _.each(appCollections, function (appCollection) {
            var remove = Meteor.wrapAsync(appCollection.remove, appCollection);
            remove({});
        });
    },
    InitData = function () {
        InitForms();
        // InitMedications();
        InitDiseases();
        InitDiseaseCategories();
       // InitProcedures();
        InitLocations();
    },
    addTestUser = function (params) {
        return Accounts.createUser({
            email: params.email,
            password: params.password,
            profile: {
                name: params.name
            }
        });
    },
    InitUsers = function() {
        if (Meteor.users.find().count() == 0) {
            addTestUser({
                email: "miniwe@mail.ru",
                password: "diamond",
                name: "Miniwe"
            });
            const userId = addTestUser({
                email: "n@1.com",
                password: "123456",
                name: "Nik"
            });
            Roles.setUserRoles( userId, 'admin' );
            addTestUser({
                email: "mni@test.com",
                password: "123456",
                name: "MNI"
            });
            addTestUser({
                email: "mohammed@test.com",
                password: "123456",
                name: "Mohammed"
            });
            addTestUser({
                email: "justin@test.com",
                password: "123456",
                name: "Justin"
            });
            addTestUser({
                email: "matt@test.com",
                password: "123456",
                name: "Matthew"
            });


        };
    };
    var InitDeseases = function() {

    }

Meteor.methods({
    initApp: function () {
        ClearData();
        InitUsers();
        InitData();
    }
});
