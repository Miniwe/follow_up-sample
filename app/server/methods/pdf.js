import Future  from 'fibers/future';
import cp      from "child_process";
import { HTTP } from 'meteor/http';

let PDFMethods = {
    'generate_pdf': function( encounterId ) {
        this.unblock();
        let future = new Future();

        let printUrl = `${Meteor.absoluteUrl()}/encounter-print/${encounterId}`,
            ext = 'pdf',
            params = {},
            questionnaire = Questionnaires.findOne({encounterId: encounterId}),
            patientName = Patients.findOne(questionnaire.patientId).name.replace(/[^a-z]/gi, '_'),
            createdDate = moment(new Date(questionnaire.createdDate)).format('YYYYMMDD');
            filename = `${questionnaire.type}__${patientName}__${createdDate}`,
            url = `https://dev.followupapp.ca/convert`;

        if (process.env.NODE_ENV == 'development') {
            url = `http://printfu.dev/convert`;
        }

        HTTP.call('GET', url, {
            rejectUnauthorized: false,
            data: {
                auth: 'arachnys-weaver',
                url:   printUrl
            }
        }, (error, response) => {
            if(error){
                console.log(error);
                throw new Meteor.Error(500, url +" failed");
            }
            future.return(response);
        });

        let pdfData = future.wait();
        let base64String = new Buffer(pdfData).toString('base64');

        return {
            filename: `${filename}.${ext}`,
            data: base64String
        };
    }
};

Meteor.methods(PDFMethods);
