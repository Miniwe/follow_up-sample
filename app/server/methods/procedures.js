let ProceduresMethods = {
  initProcedures(params) {
    var parser = new xml2js.Parser()
    var fs = Npm.require('fs');
    for (var i = 1; i < 4; i++) {
      fs.readFile(`assets/app/icd_10_pcs_2009_part${i}.xml`, Meteor.bindEnvironment(function(err, data) {
        parser.parseString(data, function (err, result) {
          result = JSON.stringify(result)
          result = JSON.parse(result);

          _.each(result.tryton.data, function (level1) {
            _.each(level1, function (level2) {
              _.each(level2, function (level3) {

                var icd = {}
                if(level3.$){
                  icd._id = level3.$.id
                }
                _.each(level3.field, function (level4) {
                  if (level4.$ && level4._ && level4.$.name == "name") {
                    icd.name = level4._
                  } else if (level4.$ && level4._ && level4.$.name == "description") {
                    icd.description = level4._
                  }

                })//END EACH level4
                if (icd._id) {
                  new Procedure(icd).save()
                }
              });//END EACH level3
            });//END EACH level2

          })//END EACH level1
        });
      }));
    }//end if
  }
}

Meteor.methods(ProceduresMethods);
