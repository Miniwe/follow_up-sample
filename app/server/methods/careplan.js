let CareplansMethods = {
  updateDiagnosis(careplanId, params) {
    var careplan = Careplans.findOne({_id: careplanId});
    if (careplan) {
      careplan.updateDiagnosis(params);
    }
  },
  modifyCareplan(params) {
    check( params, Object );
    let careplan = Careplans.findOne({_id: params.careplanId}, {_id: 0}),
        item = {
            name: params.name,
            presidingPhysician: params.presidingPhysician
        };
    if (params.related) {
      switch (params.action) {
        // case CRA_CREATENEWBASED:
          // item.name = 'Copy of ' + item.name;
          // TODO: assign forms for patient by based on careplan theartnament type
          // break;
        case CRA_SETPARENT:
          item.partOf = params.related;
          break;
        default:
          item.partOf = params.related;
      }

    }
    else {
      item.partOf = null;
    }
    careplan.update(item);
  },
  createCareplan(params) {
    check( params, Object );
    let newCareplan = Patients.findOne(params.patientId).addCareplan(params.careplanId, params.name);
    if (params.related) {
      let careplan = Careplans.findOne({_id: params.related}, {_id: 0});
      careplan["pathway"] = [];
      if (params.carePathwayStatus) {
        careplan["pathway"] = [params.carePathwayStatus];
        careplan["carePathwayStatus"] = params.carePathwayStatus;
      }
      if (params.type) {
        careplan["type"] = params.type;
      }
      delete careplan["_id"];
      delete careplan["patient"];
      delete careplan["questionnaires"];
      delete careplan["name"];
      delete careplan["treatmentOptionId"];
      delete careplan["dateOfSurgery"];

      careplan.partOf =  params.related;
      newCareplan.update(careplan);
    }
    return {
      _id: newCareplan._id,
      name: newCareplan.name
    };
  },
  archiveCareplan(params) {
    var careplan = Careplans.findOne({_id: params.careplanId});
    if (careplan) {
      careplan.archiveCareplan(params);
      return true;
    }
    else {
      return 'Careplan to archive not found';
    }
  },
  unarchiveCareplan(params) {
    var careplan = Careplans.findOne({_id: params.careplanId});
    if (careplan) {
      return careplan.unarchiveCareplan(params);
    }
    else {
      return 'Careplan to unarchive not found';
    }
  }
};

Meteor.methods(CareplansMethods);
