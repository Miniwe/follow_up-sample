import json2xml from "json2xml";
import JSZip from "jszip";

let _addFileToZipArchive  = ( archive, name, contents ) => {
  archive.file( name, contents );
};


let _addAssets = ( folder ) => {
  _addFileToZipArchive( folder, 'style.css', Assets.getText( 'export/style.css' ) );
  _addFileToZipArchive( folder, 'bootstrap.js', Assets.getText( 'export/bootstrap.js' ) );
};


let _getDataFromCollection = ( collection ) => {
  let data = collection.find( { owner: Meteor.userId() } ).fetch();
  if ( data ) {
    return data;
}
};


let _prepareDataForArchive = ( archive, collection, type, fileName ) => {
  let data          = collection instanceof Mongo.Collection ? _getDataFromCollection( collection ) : collection,
  formattedData = _formatData[ type ]( data );
  _addFileToZipArchive( archive, fileName, formattedData );
};


let _formatData = {
  csv( data )  { return Papa.unparse( data ); },
  xml( data )  { return json2xml( { 'posts': data }, { header: true } ); },
  json( data ) { return JSON.stringify( data, null, 2 ); },
  html( data ) {
    let header = ''; // Assets.getText( 'export/header.html' ),
        footer = ''; // Assets.getText( 'export/footer.html' );
        return header + data + footer;
    }
};


let _compileZip = ( archive, content ) => {
    // _addAssets( assetsFolder );
    _prepareDataForArchive( archive, content, 'csv', 'report.csv' );
    // _prepareDataForArchive( archive, Posts, 'xml', 'posts.xml' );
    // _prepareDataForArchive( archive, Comments, 'json', 'comments.json' );
    // _prepareDataForArchive( archive, content, 'html', 'profile.html' );
};


let _initializeZipArchive = () => {
    return new JSZip();
};


let _generateZipArchive = ( archive ) => {
    return archive.generate( { type: 'base64' } );
};



let _qExportData = ( options ) => {
    let archive = _initializeZipArchive();
    _compileZip( archive, options.content );
    return _generateZipArchive( archive );
};


let QExportMethods = {
    qExportData(content) {
        check(content, Object);

        try {
            return _qExportData( { content: content } );
        } catch ( exception ) {
            console.log('QExportMethods exception', exception);
            return exception;
        }

    }
};

Meteor.methods(QExportMethods);
