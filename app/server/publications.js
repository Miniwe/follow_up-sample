Meteor.publish('currentUser', function () {
    if (this.userId) {
        return Meteor.user();
    } else {
        this.ready();
    }
});

Meteor.publish('questionnaires', function(){
    return Questionnaires.find();
    this.unblock();
});

Meteor.publish('admissions', function (options) {
    options = options || {};
    return Admissions.queryResults(options);
    this.unblock();
});

// Meteor.publish('tasks', function (opt) {
//     return Tasks.find(opt);
// });

Meteor.publish('Careplans', function (opt) {
    opt = opt || {};
    return [
        Careplans.find(opt),
        CareplanDiagnosis.find()
    ];
});

Meteor.publish('CareplanDiagnosis', function (opt) {
    return CareplanDiagnosis.find();
});


Meteor.publish('Careplans._id', function (id) {
    opt = opt || {};
    opt._id = id;
    return Careplans.findOne(opt);
});

Meteor.publish('AdmissionNote', function(){
    return AdmissionNotes.find();
});

Meteor.publish('ConsultNote', function(){
    return ConsultNotes.find();
});

Meteor.publish('DischargeNote', function(){
    return DischargeNotes.find();
});

Meteor.publish('locations', function(){
    return Locations.find();
});

Meteor.publish('users.all', function(){
    return Meteor.users.find();
});

Meteor.publish( 'invites.all', function() {
  return Invitations.find();
});

Meteor.publish( 'invite', function( token ) {
  check( token, String );
  return Invitations.find( { "token": token } );
});

Meteor.publish('questionnaires.all', function(){
    return Questionnaires.find();
});
Meteor.publish('currentQuestionnaires', function(questionnaireId){
    return Questionnaires.find(questionnaireId);
});
Meteor.publish('forms.all', function(){
    return Forms.find();
});


Meteor.publish('encounters', function(){
    return Encounters.find();
});

Meteor.publish('patients', function(options){
    options = options || {};
    return Patients.find(options);
});
Meteor.publish('patients.count', function(options){
    options = options || {};
    let patients = Patients.find(options);
    Counts.publish(this, `patients.count`, patients);
    this.unblock();
});


Meteor.publish('forms', function(){
    return Forms.find();
});

Meteor.publish('reports', function(){
    return Reports.find();
});

Meteor.publish('reports.results.count', function(options){
    options = options || {};
    Counts.publish(this, `reports.results.count`, Admissions.queryResults(options));
    return undefined;
});

// Meteor.publish('reports.results', function(options){
//     options = options || {};
//     return Admissions.queryResults(options);
// });

Meteor.publish('diseases.predefined', function(){
    return Diseases.find({_id: {$in: Modules.both.getPredefinedIds()}});
});

Meteor.publish('procedures.predefined', function(){
});

Meteor.publish('diseaseCategories', function(){
    return DiseaseCategories.find();
});

Meteor.publish('diseases', function(qId){
    this.unblock();
    if (qId) {
        let q = Questionnaires.findOne(qId);
        if ((q.type == "AdmissionNote") || (q.type == "DischargeNote")) {
            return Diseases.find();
        }
    }
    return Diseases.find({_id: 'n/a'});
});
