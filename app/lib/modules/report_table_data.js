let getFieldsHeader = function(report) {
    let fieldsHeader = [
        { label: 'Patient Name', name: 'patient.name', schema: 'PatientInfoSchema'},
        { label: 'Patient Id', name: 'patient._id', schema: 'PatientInfoSchema'},
        { label: 'Careplan Id', name: 'careplanId', schema: 'PatientInfoSchema'}
    ];

    if (report) {

        if (report.admissionsCriteriа) {
            report.admissionsCriteriа.forEach((item) => {
                if (item.field != 'patientId') {
                    fieldsHeader.push({
                        label: Modules.both.getLabel(AdmissionsSchema, item.field),
                        name: item.field,
                        schema: 'AdmissionsSchema'
                    });
                }
            });
        }

        if (report.formsCriteriа) {
            fieldsHeader.push({
                label: 'Form type',
                name: 'note_form',
                schema: false
            });
            report.formsCriteriа.forEach((item) => {
                let schema = false;

                switch (item.form) {
                    case "AdmissionNote":
                        schema = AdmissionNoteSchema;
                        break;
                    case "ConsultNote":
                        schema = CNchema;
                        break;
                    case "DischargeNote":
                        schema = DischargeNoteSchema;
                        break;
                }

                if (schema && item.formСriteria) {
                    item.formСriteria.forEach((fItem) => {
                        fieldsHeader.push({
                            label:  Modules.both.getLabel(schema, fItem.field),
                            name: fItem.field,
                            schema: item.form
                        });
                    });
                }
            });

        }
    }
    return fieldsHeader;
};

let getResultsRows = function(report, results, fieldsHeader, grouped) {
    let resultsRows = [];
    grouped = grouped || false;

    results = results.fetch();

    _.each(results, (result) => {
        let baseFields = [
                {cellClass: 'PatientInfoSchema_patient.name', value: Patients.findOne(result.patientId).name},
                {cellClass: 'PatientInfoSchema_patient._id', value: result.patientId},
                {cellClass: 'PatientInfoSchema_careplanId', value: result.careplanId}
            ],
            admissionFields = [],
            forms = result.getForms(report);

        admissionFields = _.filter(fieldsHeader, (item)=>{
            return item.schema == 'AdmissionsSchema';
        });
        admissionFields = _.map(admissionFields, (item) => {
            let value = _.reduce(item.name.split('.'), (obj, item) => {

                if (item == '$') {return obj; }

                if (!_.isArray(obj)){return obj[item]; }
                else {
                    return (_.map(obj,(element) => element[item])).join(',');
                }
            }, result);
            // return value;
            return {
                cellClass: `AdmissionsSchema_${item.name}`,
                value: value
            };
        });
        baseFields = _.union(baseFields, admissionFields);


        if (forms.length) {
            forms.forEach((form) => {
                let formFields = [];


                formFields = _.filter(fieldsHeader, (item) => {
                    return item.schema == form.constructor.name;
                });
                formFields = _.map(formFields, (item) => {
                    let value = _.reduce(item.name.split('.'), (obj, item) => {

                        if (item == '$') {return obj; }

                        if (!_.isArray(obj)){return obj[item]; }
                        else {
                            return (_.map(obj,(element) => element[item])).join(',');
                        }
                    }, form);
                    // return value;
                    return {
                        cellClass: `${item.schema}_${item.name}`,
                        value: value
                    };
                });

                if (grouped) {
                    resultsRows.push(_.union(baseFields, formFields));
                }
                else {
                    resultsRows.push(_.union(baseFields, [{cellClass: 'NoteCell', value: form.constructor.name}], formFields));
                }
            });
        }
        else {
            resultsRows.push(baseFields);
        }
    });
    return resultsRows;
};

export { getFieldsHeader, getResultsRows };
