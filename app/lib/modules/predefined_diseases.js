export let predefinedDiseases = {
    reason_for_admission: [
        "C719", "C720", "E878", "G039", "G459", "G919", "J189", "I252", "I499", "I609", "I619", "I260", "M059", "R402"
    ],
    medical_history: [
    "E039","E110","E785","F039","G40909","G4733","G919","I100","I2510","I48","I609","I619","I639","J449","J459","Z720"
    ],
    Secondary_diagnosis: ["D649","E039","E10","E11","E785","I100","I48","I639","G4733","K219","M810","N400","Z720","Z950"],
    Complications_during_stay: ["I119"]
};


