import { predefinedDiseases } from './predefined_diseases.js';
import { predefinedProcedures } from './predefined_procedures.js';
import { getFieldsHeader, getResultsRows } from './report_table_data.js';

Modules = {
    client: {
        getFieldsHeader: getFieldsHeader,
        getResultsRows: getResultsRows
    },
    server: {},
    both: {
        getLabel: function (schema, field) {
            // let fieldLabel = '',
            //     parentLabel = '',
            //     parentField = '';
            // if (schema._schema[field].label) {
            //     fieldLabel = schema._schema[field].label;
            // }
            // else {
            //     fieldLabel = field;
            // }
            // parentField = field.split('.').shift();
            // if ( parentField != field) {
            //     if (schema._schema[parentField].label) {
            //         parentLabel = schema._schema[parentField].label;
            //     }
            //     else {
            //         parentLabel = parentField;
            //     }
            // }
            // else {
            //     parentField = false;
            // }
            // return (parentField) ? `${parentLabel} - ${fieldLabel}`: fieldLabel;
            return field;
        },
        getAllPredefinedIds: function() {
            let ids = [];
            _.each(predefinedDiseases, function(value, key) {
                ids.push.apply(ids, value);
            });
            return ids;
        },
        getPredefinedIds: function(name) {
            return (predefinedDiseases[name]) ? predefinedDiseases[name] : [];
        },
        getPredefinedProcedures: function(name) {
            return (predefinedProcedures[name]) ? predefinedProcedures[name] : [];
        },
    }
};