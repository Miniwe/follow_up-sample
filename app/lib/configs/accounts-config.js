AccountsTemplates.configure({
    showForgotPasswordLink: true,
    positiveValidation: true,
    negativeValidation: true,
    enablePasswordChange: true,
    negativeFeedback: true,
    positiveFeedback: true,
    confirmPassword: true,
    hideSignInLink: false,
    hideSignUpLink: false,
    showLabels: false,
    privacyUrl: "",
    termsUrl: "",
    postSignUpHook: postSignUpHook,

    texts: {
        title: {
            // signIn: "",
            // signUp: "",
            changePwd: "",
            // enrollAccount: "Enroll Title",
            // forgotPwd: "Forgot Pwd Title",
            // resetPwd: "Reset Pwd Title",
            // verifyEmail: "Verify Email Title",
        },
        button: {
            signIn: "Log in",
            signUp: "Next",
            changePwd: "Change Password"
        },
        signInLink_pre: "Already in a chat?",
        navSignIn: "signIn",
        navSignOut: "signOut",
        optionalField: "optional",
        pwdLink_pre: "",
        pwdLink_link: "forgotPassword",
        pwdLink_suff: "",
        resendVerificationEmailLink_pre: "Verification email lost?",
        resendVerificationEmailLink_link: "Send again",
        resendVerificationEmailLink_suff: "",
        // sep: "OR",
        signInLink_pre: "or",
        signInLink_link: "login",
        signInLink_suff: "",
        signUpLink_pre: "or",
        signUpLink_link: "sign up",
        signUpLink_suff: "",
        // socialAdd: "add",
        // socialConfigure: "configure",
        // socialIcons: {
        //     "meteor-developer": "fa fa-rocket",
        // },
        // socialRemove: "remove",
        // socialSignIn: "signIn",
        // socialSignUp: "signUp",
        // socialWith: "with",
        // termsPreamble: "clickAgree",
        // termsPrivacy: "privacyPolicy",
        // termsAnd: "and",
        // termsTerms: "terms"
        errors: {
            loginForbidden: "error.accounts.Authentication failed. Please try again",
        }
    }
});

var email = AccountsTemplates.removeField('email');
var password = AccountsTemplates.removeField('password');


AccountsTemplates.addField({
  _id: 'name',
  type: 'text',
  optional: false,
  displayName: 'Name'
});

AccountsTemplates.addField({
  _id: 'role',
  required: false,
  type: 'hidden'
});

AccountsTemplates.addField({
  _id: 'addSocial',
  required: false,
  type: 'hidden'
});

AccountsTemplates.addFields([
    email,
    password
]);

// AccountsTemplates.configureRoute('forgotPwd',{
//     name: 'forgotPass',
//     template: 'atForgotPwd',
//     layoutTemplate: 'applicationEmptyLayout',
// });
AccountsTemplates.configureRoute('resetPwd', {
    template: 'atResetPwd',
    layoutTemplate: 'applicationEmptyLayout',
});

AccountsTemplates.configureRoute('signUp',{
    name: 'register',
    path: '/register',
    template: 'register',
    layoutTemplate: 'applicationEmptyLayout',
    onRun: function(){
      AccountsTemplates._fields = AccountsTemplates.patientLoginFields;
      this.next()
    },
});

function postSignUpHook(userId, info){
  if (info.profile.role) {
    Roles.addUsersToRoles(userId, [info.profile.role]);
    Meteor.users.update( {_id: userId} , {$unset: { "profile.role" : "" } } );
    Meteor.users.upsert( {_id: userId} , {$set: {"profile.addSocial": true } } );
  } else {
    Roles.addUsersToRoles(userId, ['patient']);
  }
};
