usersOptions = function () {
  return Meteor.users.find({}).map(function (c) {
    return {label: c.profile.name, value: c._id};
  });
};

insertedTimestampDate = function () {
  return new Date();
};

Schemas = {};
Schemas.Forms = {};
