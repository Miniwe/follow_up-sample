InitPatients = function () {
    var patients = [];
    var users = Meteor.users.find().fetch();

    var findDoctorByName = function(name) {
        return Meteor.users.findOne( { profile: { name: name } } )._id;
    }

    PatientsJSON.forEach(function (patient) {
        var assignedToUser = findDoctorByName(patient.assignedTo);
        patients.push({
            _id: Random.id(),
            name: patient.name,
            birthDate: patient.birthDate,
            gender: patient.gender,
            phone: "",
            isActive: true,
            enrolmentDate: new Date(),
            createdDate: new Date(),
            assignedTo: patient.assignedTo,
            presidingPhysician: patient.presidingPhysician,
            carePathwayStatus: "",
            phn: patient.phn,
            csorn: patient.csorn,
        });
    });
    for (var i = 1; i < (patients.length + 1); i++) {
        var patient = patients[i - 1];
        if (!Patients.findOne({
                name: patient.name
            })) {
        var item = new Patient(patient);

        Patients.insert(item, function (err, _id) {
            if (!err) {
                var patient = Patients.findOne(_id);
                if (patient) {
                    new Patient(patient).addInitialData();
                }
            }
        });
        }
    }
};