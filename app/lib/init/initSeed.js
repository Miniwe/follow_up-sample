PatientsJSON = [
  {
    "name": "Christine Aalhus",
    "phn": "82186-2020",
    "birthDate": "",
    "gender": "Female",
    "assignedTo": "Fox",
    "presidingPhysician": "Fox",
    "csorn": 825
  },
  {
    "name": "Eva Adolph",
    "phn": "35013-7610",
    "birthDate": "",
    "gender": "Female",
    "assignedTo": "Nataraj",
    "presidingPhysician": "Nataraj",
    "csorn": 827
  },
  {
    "name": "Ian Adzich",
    "phn": "61741-9310",
    "birthDate": "",
    "gender": "Male",
    "assignedTo": "Fox",
    "presidingPhysician": "Fox",
    "csorn": 2326
  },
  {
    "name": "Danell Albert",
    "phn": "87101-0700",
    "birthDate": "",
    "gender": "Male",
    "assignedTo": "Fox",
    "presidingPhysician": "Fox",
    "csorn": 824
  },
  {
    "name": "Shelley Aldrich",
    "phn": "73861-1851",
    "birthDate": "",
    "gender": "Female",
    "assignedTo": "Nataraj",
    "presidingPhysician": "Nataraj",
    "csorn": 4579
  },
  {
    "name": "Lisa Alexandre",
    "phn": "83281-9720",
    "birthDate": "",
    "gender": "Female",
    "assignedTo": "Fox",
    "presidingPhysician": "Fox",
    "csorn": 4580
  },
  {
    "name": "Randy Allen",
    "phn": "53822-0130",
    "birthDate": "",
    "gender": "Male",
    "assignedTo": "Fox",
    "presidingPhysician": "Fox",
    "csorn": 2053
  },
  {
    "name": "Brian Anderson",
    "phn": "39760-0221",
    "birthDate": "",
    "gender": "Male",
    "assignedTo": "Nataraj",
    "presidingPhysician": "Nataraj",
    "csorn": 2183
  },
  {
    "name": "Linda Armstrong",
    "phn": "76343-7900",
    "birthDate": "",
    "gender": "Female",
    "assignedTo": "Fox",
    "presidingPhysician": "Fox",
    "csorn": 6095
  }
  
]