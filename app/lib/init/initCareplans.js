InitiCareplans = function () {
  var careplans = Careplans.find().fetch();
  for (var i = 0; i < 5; i++) {
    careplan = careplans[i];
    if (careplan) {
      var option = TreatmentOptions.findOne({
        name: "No Follow-up required / Discharge patient"
      });
      careplan.treatmentOptionId = option._id;
      careplan.save();
      careplan.process();
    }
  }

}
