InitLocations = function() {

    new Location({
        name: 'Montreal Neurological Institute',
        type: LT_HOSPITAL,
        province: 'Quebec',
        city: 'Montreal',
        location_number: '0019X',
        functional_centres: [
            { code: '0', name: 'Toxicology', french_name: 'Toxicologie' },
            { code: '1', name: 'inpatient', French_name: 'Clinique externe' },
            { code: '2', name: 'Other', french_name: 'Autres' },
            { code: '3', name: 'Surgery', french_name: 'Soins généreaux' },
            { code: '4', name: 'Long-term care', french_name: 'hébergement' },
            { code: '5', name: 'Clinical Laboratory', french_name: 'Laboratoire'},
            { code: '6', name: 'ICU', french_name: 'Soins intensifs ou soins coronariens' },
            { code: '7', name: 'Emergency', french_name: 'Urgence' },
            { code: '8', name: 'Psychiatry', french_name: 'Département de psychiatrie'},
        ]
    }).save();


};
