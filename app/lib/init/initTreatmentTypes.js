ttTypes = new Array();
ttTypes[TT_NO_REQUIRED = 0] = 'No further treatment required';
ttTypes[TT_FOLLOWUP    = 2] = 'Follow-up required only';
ttTypes[TT_SURGERY     = 3] = 'Surgery and follow-up';
ttTypes[TT_NO_ACCESS   = 4] = 'Cannot assess / confirmation needed / To be determined';
ttTypes[TT_ANOTHER     = 5] = 'Refer patient to another provider';

InitTreatmentTypes = function () {
  var treatmentTypes = ttTypes,
    forms = _.map(Forms.find().fetch(), function(value, index){
      return value;
    });

  for (var i = 1; i < treatmentTypes.length + 1; i++) {
    var name = treatmentTypes[i - 1];
    var treatmentTypeId = Random.id();
    new TreatmentType({
      _id: treatmentTypeId,
      name: name
    }).save();
    if (i == 2) {
      new TreatmentOption({
        name: "No Follow-up required / Discharge patient",
        treatmentTypeId: treatmentTypeId,
        forms: [_.findWhere(forms, {
          type: 'Discharge'
        })._id],
        carePathwayStatus: PS_DISCHARGE_NO_FURTHER_TREATMENT_REQUIRED
      }).save();
    }

  }
}
