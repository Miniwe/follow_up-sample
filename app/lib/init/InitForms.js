InitForms = function () {
  // new Form({
  //   type: "InitialClinicalAssessment",
  //   name: "Initial clinical assessment form",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();  
  // new Form({
  //   type: "SpineSurgicalProcedure",
  //   name: "Spine surgical form",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();

  // new Form({
  //   type: "Discharge",
  //   name: "Discharge Form",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();

  // new Form({
  //   type: "ASIASpinalExam",
  //   name: "ASIA / Spinal Examination Form",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();

  // new Form({
  //   type: "ReferralInbound",
  //   name: "Inbound Referral Form",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();

  //   new Form({
  //   type: "ReferralOutbound",
  //   name: "Outbound Referral Form",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();

  // new Form({
  //   type: "Spine3MonthFollowUp",
  //   name: "3 Month Follow-up form",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();

  // new Form({
  //   type: "SpinePre12WksAdverseEvent",
  //   name: "Pre 12 Weeks Adverse Event",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();

  // new Form({
  //   type: "Spine12MonthFollowUp",
  //   name: "12 Month Follow-up form",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();

  // new Form({
  //   type: "Spine24MonthFollowUp",
  //   name: "24 Month Follow-up form",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();

  //   new Form({
  //   type: "HospitalFollowUp",
  //   name: "Hospital Follow-up Form",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();

  // new Form({
  //   type: "PatientGeneralAssessment",
  //   name: "General Clinical Assessment Form",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();
  // new Form({
  //   type: "PatientInitialInterview",
  //   name: "Patient Interview form",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();  new Form({
  //   type: "PatientConsent",
  //   name: "Patient Consent Form",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();
  // new Form({
  //   type: "Patient24MonthFollowUp",
  //   name: "Patient 24 Month FollowUp Form",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();
  //   new Form({
  //   type: "Patient12MonthFollowUp",
  //   name: "Patient 12 Month FollowUp Form",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();
  // new Form({
  //   type: "Patient3MonthFollowUp",
  //   name: "Patient 3 Month FollowUp Form",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();
  // new Form({
  //   type: "PatientInitialAssessment",
  //   name: "Patient Initial Assessment Form",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();
  // new Form({
  //   type: "PatientSurgery",
  //   name: "Patient Pre-Surgery Form",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();
  // new Form({
  //   type: "EndOfStudy",
  //   name: "End of study",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'default'
  // }).save();

  new Form({
    type: "ConsultNote",
    name: "Consult Note",
    documentId: "FMU-0339",
    questions: [{
      name: "sample question"
    }],
    'class': 'post-surgery'
  }).save();    

  new Form({
    type: "DischargeNote",
    name: "Discharge Note",
    documentId: "FMU-0385",
    questions: [{
      name: "sample question"
    }],
    'class': 'post-surgery'
  }).save();  
  new Form({
    type: "AdmissionNote",
    name: "Admission note",
    documentId: "FMU-0656",
    questions: [{
      name: "sample question"
    }],
    'class': 'post-surgery'
  }).save();  

  // new Form({
  //   type: "CNS",
  //   name: "CNS",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'post-surgery'
  // }).save();

  // new Form({
  //   type: "CVS",
  //   name: "CVS",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'post-surgery'
  // }).save();

  // new Form({
  //   type: "Respiratory",
  //   name: "Respiratory",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'post-surgery'
  // }).save();
  // new Form({
  //   type: "Renal",
  //   name: "Renal",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'post-surgery'
  // }).save();

  // new Form({
  //   type: "Hematology",
  //   name: "Hematology",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'post-surgery'
  // }).save();

  // new Form({
  //   type: "Procedure",
  //   name: "Procedure",
  //   questions: [{
  //     name: "sample question"
  //   }],
  //   'class': 'post-surgery'
  // }).save();

}
