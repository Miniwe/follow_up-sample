Theme = class Theme {
  constructor(doc) {
    _.extend(this, doc);
  }
  getRouteTheme(routeName) {
    switch (routeName) {
      case "surgericals":
        return {
          color: 'blue',
          direction: 'inverted',
          isCurrent: Router.current().params.query["type"] === '["Surgery wait list"]'
        };
        break;
      case "patients":
        return {
          color: 'blue',
          direction: 'inverted',
          isCurrent: Router.current().route.getName() === routeName
        };
        break;
      case "careplans":
        return {
          color: 'orange',
          direction: 'inverted',
          isCurrent: Router.current().route.getName() === routeName
        };
        break;
      case "clinicalForms":
        return {
          color: 'red',
          direction: 'inverted',
          isCurrent: Router.current().route.getName() === routeName
        };
        break;
      case "billingsheets":
        return {
          color: 'teal',
          direction: 'inverted',
          isCurrent: Router.current().route.getName() === routeName
        };
        break;
      case "lists":
        return {
          color: 'grey',
          direction: 'inverted',
          isCurrent: Router.current().route.getName() === routeName
        };
        break;
      case "appointments":
        return {
          color: 'pink',
          direction: 'inverted',
          isCurrent: Router.current().route.getName() === routeName
        };
      case "admissions":
        return {
          color: 'violet',
          direction: 'inverted',
          isCurrent: Router.current().route.getName() === routeName
        };
      case "reports":
        return {
          color: 'green',
          direction: 'inverted',
          isCurrent: Router.current().route.getName() === routeName
        };
        break;
      default:
        return {
          color: 'black',
          direction: 'inverted',
          isCurrent: Router.current().route.getName() === routeName
        };

    }
  }
}
