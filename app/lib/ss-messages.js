Meteor.startup(function() {
  SimpleSchema.messages({
    'checkedIn': 'Patient is checked in. Must be checked out before another check in'
  });
});
