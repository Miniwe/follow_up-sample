import { FastClick } from 'fastclick';

Meteor.startup(() => {
    AutoForm.setDefaultTemplate("semanticUI");
    document.addEventListener("touchstart", () => {}, true);
    // Waves.attach('button.ui', ['waves-button'])

    // OneSignal settings
    // Add to index.js or the first page that loads with your app.
    // For Intel XDK and please add this to your app.js.
    document.addEventListener('deviceready', function () {
        // Enable to debug issues.
        // window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
        var notificationOpenedCallback = function (jsonData) {
            console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
        };

        window.plugins.OneSignal.init("ec61e64e-5857-4f65-848d-e7e628a53f58", {
                googleProjectNumber: "678557088395"
            },
            notificationOpenedCallback);

        // Show an alert box if a notification comes in when the user is in your app.
        window.plugins.OneSignal.enableInAppAlertNotification(true);

    }, false);

    (typeof FastClick) && FastClick.attach(document.body);
});

String.prototype.supplant = function (o) {
    return this.replace(/{([^{}]*)}/g,
        (a, b) => {
            const r = o[b];
            return typeof r === 'string' || typeof r === 'number' ? r : a;
        }
    );
};

App = {};

App.success = (
    payload = {
        duration: 1000
    }) => {
    Dispatch("DIMMER_ENABLED", payload);
}

App.confirm = (payload = {}) => {
    Dispatch("CONFIRM_UPDATE", payload).then("CONFIRM_ENABLED");
}

App.alert = params => {
    const defaultParams = {
        closable: true,
        onDeny: function () {},
        onApprove: function () {}
    };
    if (typeof params == 'string') {
        Session.set('alert-title', params);
        params = {};
    } else {
        Session.set('alert-title', params.message || 'Alert');
        Session.set('alert-buttons', params.buttons || false);
    }
    setTimeout(function () {
        $('.alert.modal')
            .modal(_.extend({}, defaultParams, params))
            .modal("show");
    }, 400);
}

App.showSnackbar = data => {
    let snackbar;

    const defaultData = {
        message: 'Record updated',
        timeout: 2000
    };

    if (snackbar = document.querySelector('.mdl-js-snackbar')) {
        snackbar.MaterialSnackbar.showSnackbar(_.extend({}, defaultData, data));
    }
};

// Template.onRendered(function () {
//   // Initialize all datepicker inputs whenever any template is rendered
//     this.$('.ui.accordion').accordion();
// });

Template.onRendered(function () {
    $('div.ui.secondary.bottom.attached.form').addClass('fields');
    $(".array_adjust").parent('.field').addClass('ten wide');
});
