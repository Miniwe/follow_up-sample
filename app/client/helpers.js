// stopping reloads on file changes and calling refreshPage after initial app is loaded,
// i.e after the user has loaded the app, and has changed some file
Reload._onMigrate("LiveUpdate", function () {
  Deps.autorun(function () {
    Autoupdate.newClientAvailable();
    console.log("Updated on" + moment().format('MMMM Do YYYY, HH:mm:ss a'));
    // LiveUpdate.refreshPage();
  });
  return [false];
});

UI.registerHelper('currentRoute', function(routeName) {
	return Router.current().route.getName() == routeName;
})
