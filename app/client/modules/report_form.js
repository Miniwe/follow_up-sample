let reportForm = function(event, encounterId) {
    event.preventDefault();
    let printUrl = `https://mni.followupapp.ca/encounter-print/${encounterId}`;
        serviceUrl = `https://dev.followupapp.ca/convert?auth=arachnys-weaver&url=${printUrl}`;

    App.showSnackbar({
        message: 'Print PDF started',
        timeout: 3000
    });
    window.open(serviceUrl);
    // $(event.target).addClass('blue disabled');

    // Meteor.call('generate_pdf', encounterId, function(err, res) {
    //     if (err) {
    //         console.error(err);
    //         App.showSnackbar({
    //             message: 'Error: Print PDF',
    //             timeout: 3000
    //         });
    //     } else if (res) {
    //         // window.open("data:application/pdf;base64, " + res.data);
    //         // or try this
    //         let blob = Modules.client.convertBase64ToBlob( res.data );
    //         saveAs( blob, res.filename );
    //         App.showSnackbar({
    //             message: 'Print PDF completed',
    //             timeout: 2000
    //         });
    //     }
    //     $(event.target).removeClass('blue disabled');
    // })
};

Modules.client.reportForm = reportForm;
