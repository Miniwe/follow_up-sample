let convert = ( base64String ) => {
  let decodedString       = _decodeBase64( base64String ),
      decodedStringLength = _getLength( decodedString ),
      byteArray           = _buildByteArray( decodedString, decodedStringLength );

  if ( byteArray ) {
    return _createBlob( byteArray );
  }
};

function utf8_to_b64(str) {
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
        return String.fromCharCode('0x' + p1);
    }));
}


let _decodeBase64 = ( string ) => {
  return atob( string );
  // return utf8_to_b64( string );
};


let _getLength = ( value ) => {
  return value.length;
};


let _buildByteArray = ( string, stringLength ) => {
  let buffer = new ArrayBuffer( stringLength ),
      array  = new Uint8Array( buffer );

  for ( let i = 0; i < stringLength; i++ ) {
    array[ i ] = string.charCodeAt( i );
  }

  return array;
};


let _createBlob = ( byteArray ) => {
  return new Blob( [ byteArray ], { type: 'application/zip' } );
};

Modules.client.convertBase64ToBlob = convert;
