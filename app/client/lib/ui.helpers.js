ui = {};
ui.helpers = {};
ui.helpers.form = {
  addError(form, fieldname, message) {
    form.find(`:input[name='${fieldname}']`).closest(".field").addClass("error");
    form.form("add errors", message);
  }
};

UI.registerHelper('formatDate', (date, format) => moment(new Date(date)).format(format));

UI.registerHelper('debug', (label='', value) => {
  console.log('debug', label, value);
});

UI.registerHelper('pathwayStatusCaption', (code) => {
  let caption = pathwayStatus[code];
  return caption ? caption : pathwayStatus[PS_UNDIAGNOSED];
});

UI.registerHelper('notEmpty', (object) => {
    if (object instanceof Meteor.Collection.Cursor) {
        object = object.fetch();
    }
    return _.size(object);
} );

UI.registerHelper('getCareplan', (_id, fields) => {
    let careplan = Careplans.findOne(_id);
    if (careplan) {
        if (fields && (fields = fields.split(','))) {
            if (fields.length > 1) {
                careplan = _.pluck(careplan, fields);
            }
            else {
                careplan = careplan[fields[0]];
            }
        }
    }
    return careplan;
});

UI.registerHelper('currentUser', () => {
    return Meteor.user();
});
UI.registerHelper('getUserObject', (_id, fields) => {
    let user = Meteor.users.findOne(_id);
    if (user) {
        if (fields && (fields = fields.split(','))) {
            if (fields.length > 1) {
                user = _.pluck(user, fields);
            }
            else {
                user = user[fields[0]];
            }
        }
    }
    return user;
});
