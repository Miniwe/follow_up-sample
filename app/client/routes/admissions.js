Router.map(function(){
    const controller = 'admissionsController';
    const controllerNoTabs ='admissionsNewController';
    this.route('admissions', {
        path: '/admissions',
        controller: controller
    });
    this.route('admissions.list', {
        path: '/admissions/list/:list',
        controller: controller
    });
    this.route('admission.new', {
        path: '/admissions/new',
        controller: controllerNoTabs,
        action: 'new',
        name: 'New admission'
    });
    this.route('admission', {
        path: '/admissions/:_id',
        controller: controller,
        action: 'view'
    });
    this.route('admission.edit', {
        controller: controller,
        path: '/admissions/:_id/edit',
        action: 'edit'
    });
    this.route('admission.discharge', {
        controller: controller,
        path: '/admissions/:_id/discharge',
        action: 'discharge',
        name: 'Discharge Admission'
    });
});
