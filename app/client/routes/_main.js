Router.route('dashboard', {
  path: '/dashboard',
  fastRender: true
});

// Router.route('/clinicalForms', {
//   controller: 'questionnairesController' //questionnairesController
// });

// Router.route('/clinicalForms/:_id', function () {
//   this.render('clinicalForms_detailsView', {
//     data() {
//       return Questionnaires.findOne(this.params._id);
//     }
//   });
// });

Router.route('/profile', {
  waitOn: function() {
    return {
      ready: function() {
        return Meteor.user()
      }
    }
  }
});

// Router.route('/reports', {
//   action() {
//     this.render("ReportsQuestionnaires", {
//       data() {
//         return {}
//       }
//     });
//   }
// });

// Router.route('/overduedEmail/:_id', {
//   template: "emailLayout",
//   layoutTemplate: "emailWrapper",
//   data() {
//       var data = {},
//           questionnaires = Questionnaires.getOverdued(),
//           patients = _.groupBy(questionnaires, function(questionnaire){
//             return questionnaire.patientId;
//           });
//       _.each(patients, function(patientData, key){
//         if (key == this.params._id) {
//           data = Patients.findOne({_id: key}).overduedEmail(patientData);
//         }
//       }, this);
//       return {
//         emailTemplate: "patientOverduedEmail",
//         data: data
//       };
//   }
// });

// Router.route('revisions', {
//     path: '/revisions/:collection/:_id',
//     template: 'revisions',
//     data: function() {
//         let collectionName = this.params.collection,
//             _id =  this.params._id;
//         return Meteor.connection._mongo_livedata_collections[collectionName].findOne(_id);
//     }
// });
