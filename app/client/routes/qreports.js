Router.map(function(){
    const controller = 'reportsController';
    this.route('reports', {
        path: '/reports',
        controller: controller,
        action: 'listAction',
        fastRender: true,
        waitOn() {
          return [
            Meteor.subscribe('reports')
          ]
        }
    });
    this.route('report', {
        path: '/reports/:_id',
        controller: controller,
        action: 'viewAction'
    });
    this.route('report.export', {
        path: '/reports/:_id/:format',
        controller: controller,
        action: 'exportAction'
    });
});
