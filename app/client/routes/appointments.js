Router.route('/appointments/new', {
  action: function () {
    this.render("newAppointment", {
      data: function () {
        return {}
      }
    });
  }
});

Router.route('/appointments', {
  controller: 'appointmentsController'
});

Router.route('/appointments/:_id/edit', {
  controller: 'appointmentController'
});
