State.modify('Cart.items', (state = []) => {
  switch (Action.type()) {
    case 'CART_FILTERED':
      return Cart.find({
        category: Action.category
      });
    case 'CART_NOT_FILTERED':
      return Cart.find({});
    default:
      return state;
  }
});
CareplanStore = function () {
    var self = this;
    self.get = {
      list: function () {
        var result = Careplans.find({}).fetch();
        return result;
      },
      isReady: function () {
        return false;

      }
    }
    Register(function (payload) {
      switch (Action.type()) {
        case "careplans.get":
          State.modify('Careplans', (state = false) => {
            return self.get.list();
          });
          break;
        case "careplans.isReady":

          break;
      }
    });
  }
  // CareplanStore = new CareplanStore();
