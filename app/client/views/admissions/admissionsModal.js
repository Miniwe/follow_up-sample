
Template.admissionsModal.onCreated(function(){
    let instance = this,
        admission;

    switch (Router.current().route.getName()) {
        case 'appointments':
            // admission = (this.data._id) ? Admissions.findOne({appointmentId: this.data._id}) : false;
            break;
        case 'patient':
            let patientId = Router.current().params._id;
                admission = Admissions.findOne({
                    patientId: patientId,
                    checkedIn: {$ne: null},
                    checkedOut: null
                });
            break;
        case 'admission':
            admission = Admissions.findOne({
                _id: Router.current().params._id,
                checkedIn: {$ne: null},
                checkedOut: null
            });
            break;
        case 'admissions':
                admission = Admissions.findOne({
                    patientId: this.data._id,
                    checkedIn: {$ne: null},
                    checkedOut: null
                });
            break;
    }

    if (admission) {
        instance.admission = new ReactiveVar(admission);
        instance.admissionFormTemplate = new ReactiveVar('admissionFormUpdate');
    }
    else {
        if (Router.current().route.getName() == 'appointments') {
          instance.admission = new ReactiveVar({
            appointment: instance.data
          });
        }
        else {
          instance.admission = new ReactiveVar({
            appointment: {
                subject: Router.current().params.patientId
            }
          });
        }
        instance.admissionFormTemplate = new ReactiveVar('admissionFormInsert');
    }
});

// Template.admissionsModal.helpers({
//   title() {
//     let title = 'Check-in / Admit Patient',
//         admission = Template.instance().admission.get();
//     if (admission && admission.checkedIn) {
//       title = 'Discharge Patient';
//     }
//     return title;
//   },
//   admissionFormTemplate() {
//     return Template.instance().admissionFormTemplate.get();
//   },
//   admission() {
//     let data = {admission: Template.instance().admission.get()};
//     return data;
//   }
// });

Template.admissionFormInsert.onCreated(function() {
    let instance = this,
        appointment = instance.parent().parent().parent().data;
    if ((Router.current().route.getName() == 'patient') && (!appointment || !appointment.suggest)) {
      appointment = {
          subject: Router.current().params._id
      };
    }
    instance.appointment = new ReactiveVar(appointment);
});

Template.admissionFormInsert.events({
    'click .ui.deny'(event, instance) {
        if (!$('.ui.deny').closest('.ui.modal').length) {
            history.back();
        }
    }
});

Template.admissionFormInsert.helpers({
  appointment() {
    return Template.instance().appointment.get();
  },
  defaultDateTime() {
    return moment().format("YYYY-MM-DDTHH:mm")
  }
});

Template.admissionFormUpdate.helpers({
  patientName() {
    let patient, name = 'Patient No Found';
    const admission = this.admission;
    if (admission && admission.patientId) {
        patient = Patients.findOne(this.admission.patientId);
        if (patient) {
            name = patient.name;
        }
    }
    return name;
  }
});

Template.admissionFormUpdate.helpers({
    defaultDateTime() {
      return moment().format("YYYY-MM-DDTHH:mm")
    },
    admission() {
      // The data comes in wrong format, need to check admission object
      const admission = Template.instance().data.admission;
     // delete admission._id;
      admission.checkedOut = new Date(admission.checkedOut);
      return admission;
    }
});

Template.admissionFormUpdate.events({
  'click .cancel': function () {
    window.history.back();
  }
});
