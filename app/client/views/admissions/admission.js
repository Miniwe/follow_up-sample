


AutoForm.addHooks(['insertAdmissionForm', 'updateAdmissionForm'],{
    onSuccess(formType, result) {
        let message = 'Patient discharged',
            admission = Admissions.findOne(result);

        if (admission && admission.checkedIn && !admission.checkedOut) {
            message = 'Patient admitted';
            Router.go('admission', {_id: admission._id});
        } else if (admission && admission.checkedIn && admission.checkedOut) {
            message =  'Patient discharged';
            Router.go('patient', {_id: admission.patientId});
        }

        App.showSnackbar({
            message: message,
            timeout: 2000
        });
    }
});

Template.admission.helpers({
    encounters() {
        return Encounters.find({admissionId: this._id}, {
          sort: {
            startDate: -1
          }
        });
    },
    patientData() {
      return Patients.findOne({_id: this.patientId});
    },
    admissionData() {
      return Admissions.findOne({_id: this._id});

    }
});



// Template.admission.onRendered( function () {
//   if(Router.current().route.getName() === 'admission') {
//     $('div.mdl-layout__drawer-button').hide()
//   } else {
//     $('div.mdl-layout__drawer-button').show()
//   }
// });
