Template.admission.onRendered( function () {
$('.ui.dropdown').dropdown();
});

Template.admissionNew.events({
    'click .cancel': function (event) {
        event.preventDefault();
        window.history.back();
    },
    'change [name=patientId]': function (event, instance) {
        event.preventDefault();
        const val  = $(event.target).val();
        if (val == 'new') {
            Router.go('New patient');
            return false;
        }
    }
});