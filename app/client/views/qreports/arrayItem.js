Template.arrayItem.onCreated(function(){
    let instance = this;

    instance.field = new ReactiveVar();

});


Template.arrayItem.events({
    'change .itemField .ui.dropdown'(event, instance){
        let field = $(event.target).val();
        instance.field.set(field);
    }
});


Template.arrayItem.onRendered(function(){
    this.$('input[type=hidden]', '.ui.dropdown').each( (i, el) => {
        let $el = $(el), val = $el.val();
        if (val) {
            setTimeout(function(){
                $el.parents('.ui.dropdown').dropdown('clear');
                $el.parents('.ui.dropdown').dropdown('set selected', val);
            }, 200);
        }
    });
});


Template.arrayItem.helpers({
    fieldParams() {
        let schema,
            fieldName = Template.instance().field.get(),
            schemaType = 'String',
            type = 'text',
            options = undefined;

        // получить схему
        if (this.current.field.substr(0,18) == "admissionsCriteriа") {
            schema = AdmissionsSchema;
        }
        else if (this.current.field.match(/formsCriteriа.\d+/)) {
            let noteFormName = this.current.field.match(/formsCriteriа.\d+/) + '.form',
                schemaName = AutoForm.getFieldValue(noteFormName);
            switch (schemaName) {
                case 'AdmissionNote':
                    schema = AdmissionNoteSchema;
                    break;
                case 'ConsultNote':
                    schema = CNchema;
                    break;
                case 'DischargeNote':
                    schema = DischargeNoteSchema;
                    break;
            }
        }

        if (schema && fieldName && schema._schema[fieldName]) {
            if (schema._schema[fieldName].autoform) {
                type = schema._schema[fieldName].autoform.type;
                if (schema._schema[fieldName].autoform.options) {
                    options = schema._schema[fieldName].autoform.options();
                }
            }
            else {
                switch (schema._schema[fieldName].type) {
                    case Number:
                        type = "number";
                        schemaType = 'Number';
                        break;
                    case Date:
                        type = "date";
                        schemaType = 'Date';
                        break;
                }
            }
        }

        switch (type) {
            case 'textarea':
                type = 'text';
                break;
            case 'datetime-local':
                type = 'date';
                schemaType = 'Date';
                break;
        }

        return {
            type: type,
            schemaType: schemaType,
            options: options
        };
    }
});