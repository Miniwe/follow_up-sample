const PAGE_SIZE = 10;

Template.reportResults.onCreated(function(){
    let instance = this,
        conditions = {},
        params = {
            limit: PAGE_SIZE
        };

    instance.currentPage = new ReactiveVar(1);

    instance.results = (limit) => {
        params.limit = limit || PAGE_SIZE;
        params.skip = (instance.currentPage.get() - 1) * params.limit;
        return Admissions.queryResults({reportId: Session.get('current_report')}, params);
    };

    instance.autorun(() => {
        let cr = Session.get('current_report');
        if (cr) {
            instance.subscribe('admissions', {reportId: cr});
            instance.subscribe('reports.results.count', {reportId: cr});
        }
    });

});


Template.reportResults.events({
    'click .ui.pagination a'(event, instance) {
        event.preventDefault();
        instance.currentPage.set(parseInt($(event.target).text(),10));
    }
});



Template.reportResults.helpers({
    report() {
        return (Session.get('current_report')) ? Reports.findOne(Session.get('current_report')): false;
    },
    results() {
        return Template.instance().results();
    },
    pagesCount() {
        let pc = Math.ceil(Counts.get(`reports.results.count`) / PAGE_SIZE);
        return pc;
    },
    currentPage() {
        let cp = Template.instance().currentPage.get();
        return cp ;
    }
});
