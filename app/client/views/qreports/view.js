Template.reportView.onCreated(function(){
    let instance = this;

    instance.reportId = new ReactiveVar( Session.get('current_report') );

});


Template.reportView.onRendered(function(){
    this.$('.ui.accordion').accordion();
});


Template.reportView.events({
    "click .js-export"(event, instance) {
        event.preventDefault();
        $(event.target).addClass('loadind');

        let report = Reports.findOne(Session.get('current_report')),
            header = Modules.client.getFieldsHeader(report),
            results = Admissions.queryResults({reportId: report._id}, {}),
            headerFields = _.map(header, (item) => item.label),
            resultsRows = Modules.client.getResultsRows(report, results, header);

        Meteor.call( 'qExportData', {fields: headerFields, data: resultsRows}, ( error, response ) => {
            let fileName = "report_"+ moment().format("DDMMMYYYY_HHmm");
            if ( error ) {
                console.log( 'qExportData', error );
                alert( error.reason );
            } else {
                if ( response ) {
                    let blob = Modules.client.convertBase64ToBlob( response );
                    saveAs( blob, `${fileName}.zip` );
                }
            }
            $(event.target).removeClass('loading');
        });
    },
    "click .js-remove"(event, instance) {
        event.preventDefault();
        let reportId = Session.get('current_report');

        $('.confirm.modal')
        .modal({
            closable: false,
            onDeny() {},
            onApprove() {
                $('.ui.dropdown').dropdown('clear');
                AutoForm.resetForm('ReportForm');
                $('.ui.dropdown', '.ui.secondary.menu').dropdown('restore defaults');
                Session.set("current_report", '');
                Reports.remove(reportId);
            }
        })
        .modal("show");

    }
});

Template.reportView.helpers({
    form() {
        let report,
            reportId =  Session.get('current_report');
        let form = {
            name: 'New Report',
            reportId: false,
            type: 'insert'
        };
        if (reportId) {
            report = Reports.findOne(reportId);
            if (report) {
                form = {
                    name: report.name,
                    report: report,
                    type: 'update'
                };
            }
        }
        setTimeout(() => {
            AutoForm.resetForm('ReportForm');
        }, 0);
        return form;
    }
});


Template.afArrayField_formsCriteriа.events({
    'change .noteForm .ui.dropdown'(event, instance){
        // instance.$(".noteFields").children().remove(); // uncomment abd fix @todo
    }
});


AutoForm.addHooks(['ReportForm'], {
    onSuccess(formType, result) {
        if (formType == 'insert') {
            Session.set('current_report', result);
        }
    },
    formToModifier: function(modifier) {
        if (modifier.$set.formsCriteriа) {
           modifier.$set.formsCriteriа = _.compact(modifier.$set.formsCriteriа);
        }
        return modifier;
    }
});
