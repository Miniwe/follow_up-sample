Template.reportsNav.onCreated(function(){
    let instance = this;
    instance.autorun(() => {
    });
});


Template.reportsNav.onRendered(function(){
    let $dropdown = this.$('.ui.dropdown');
    $dropdown.dropdown({
        onChange: function(val) {
            Session.set("current_report", val);
        }
    });
});


Template.reportsNav.helpers({
    reports() {
        let ql  = Reports.find({});
        return ql.count() ? ql : false;
    }
});


Template.reportsNav.events({
    'click .js-add-new'(event, instance) {
        Session.set("current_report", '');
         $('.ui.dropdown').dropdown('clear');
         AutoForm.resetForm('ReportForm');
        instance.$('.ui.dropdown').dropdown('restore defaults');
    }
});
