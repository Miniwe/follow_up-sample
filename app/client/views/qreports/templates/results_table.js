let formatResults = function (fieldsHeader, results){
    let resultsRows = [];
    results.forEach((rowItem) => {
        let row = [];
        fieldsHeader.forEach((colItem) => {
            let cellData;
            if (colItem.name == 'note_form') {
                cellData = _.find(rowItem, (item) => {
                    return item.cellClass == "NoteCell";
                });
            }
            else {
                cellData = _.find(rowItem, (item) => {
                    return item.cellClass == `${colItem.schema}_${colItem.name}`;
                });
            }
            row.push(cellData);
        });
        resultsRows.push(row);
    });

    return resultsRows;
};

Template.results_table.onCreated(function(){
    let instance = this;

    instance.report = function() {
        return Reports.findOne(Session.get('current_report'));
    };

    instance.fieldsHeader = function() {
        let report = instance.report(),
            fieldsHeader = Modules.client.getFieldsHeader(report);

        return fieldsHeader;
    };
});


Template.results_table.helpers({
    fieldsHeader() {
        return Template.instance().fieldsHeader();
    },
    getResultsRows(results, fieldsHeader) {
        let rr = Modules.client.getResultsRows(Template.instance().report, results, fieldsHeader);
        rr = formatResults(fieldsHeader, rr);
        return rr;
    }
});