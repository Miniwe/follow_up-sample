let makeGroupHeader = function(fieldsHeader) {
    let notes = [],
        fieldsHeaderRows = [];

    fieldsHeader.forEach((field, index) => {

        if (!field.schema) {
            delete fieldsHeader[index];
        }
        else if (_.find(['AdmissionNote' , 'ConsultNote' , 'DischargeNote'], (item) => {return item == field.schema})) {
            notes.push(field);
            delete fieldsHeader[index];
        }
    });

    if (notes.length) {
        fieldsHeaderRows[0] = [];
        fieldsHeader.forEach((item) => {
            fieldsHeaderRows[0].push(_.extend(item, {rowspan: 2 }));
        });

        let grNotes = _.groupBy(notes, (item) => item.schema);
        fieldsHeaderRows[1] = [];
        _.each(grNotes, (item, key) => {
            fieldsHeaderRows[0].push({
                label: key,
                name: key,
                colspan: item.length
            });
            item.forEach((ii) => {fieldsHeaderRows[1].push(ii); });
        });
    }
    return fieldsHeaderRows;
};

let formatGroupResults = function (fieldsHeader, results){
    let resultsRows = [];
    results.forEach((rowItem) => {
        let row = [];
        fieldsHeader.forEach((colItem) => {
            if (colItem.schema) {
                let cellData = _.find(rowItem, (item) => {
                    return item.cellClass == `${colItem.schema}_${colItem.name}`;
                });
                row.push(cellData);
            }
        });
        resultsRows.push(row);
    });

    return resultsRows;
};

Template.grouped_results_table.onCreated(function(){
    let instance = this;

    instance.report = function() {
        return Reports.findOne(Session.get('current_report'));
    };

    instance.fieldsHeader = function() {
        let report = instance.report(),
            fieldsHeader = Modules.client.getFieldsHeader(report);

        return fieldsHeader;
    };
});


Template.grouped_results_table.helpers({
    fieldsHeader() {
        return Template.instance().fieldsHeader();
    },
    fieldsHeaderGrouped( fieldsHeader ) {
        return makeGroupHeader(fieldsHeader);
    },
    getResultsRows(results, fieldsHeader) {
        let rr = Modules.client.getResultsRows(Template.instance().report, results, fieldsHeader, true);
        rr = formatGroupResults(fieldsHeader, rr);
        return rr;
    }
});