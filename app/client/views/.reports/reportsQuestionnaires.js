Template.ReportsQuestionnaires.events({
    'click ._download-report': function(event, instance) {
    }
});

Template.ReportsQuestionnaires.helpers({
  settings: function () {
    var fs = {
        patient: {
            fieldId: 'patientId',
            key: 'patientId',
            label: 'Patient',
            fn: function (value) {
                return Patients.findOne(value).name;
            }
        },
        name: {
            fieldId: 'name',
            key: 'name',
            label: 'Name',
            fn: function (value) {return value; }
        },
        type: {
            fieldId: 'type',
            key: 'type',
            label: 'Type',
            fn: function (value) {return value; }
        },
        dueDate: {
            fieldId: 'dueDate',
            key: 'dueDate',
            label: 'Due Date',
            fn: function (value) {
                return moment(value).format("DD.MM.YY HH:mm");
            }
        },
        status: {
            fieldId: 'status',
            key: 'status',
            label: 'Status',
            fn: function (value) {return value; }
        },
        state: {
            fieldId: 'state',
            label: 'State',
            fn: function (value, object) {
                var stateHTML = '';
                if (object.status == 'completed') {
                    stateHTML = Blaze.toHTMLWithData(Template.rq_Completed, {
                      completedBy: Meteor.users.findOne(object.completedBy).profile.name
                    });
                }
                else {
                    if (object.dueDate) {
                        stateHTML = Blaze.toHTMLWithData(Template.rq_CompletedWithin, {
                          completedWithin: moment(object.dueDate).fromNow()
                        });
                    }
                }
                return new Spacebars.SafeString(stateHTML);
            }
        },
        link: {
            fieldId: 'link',
            key: '_id',
            label: 'Link',
            fn: function (value, object) {
                return new Spacebars.SafeString(Blaze.toHTMLWithData(Template.rq_Link, {
                  _id: value
                }));
            }
        }
    };

    return {
        collection: Questionnaires,
        fields: [fs.patient,  fs.name,  fs.type, fs.dueDate, fs.status, fs.state, fs.link]
    }
  }
});
