Template.cancelAppointment.helpers({
  selectedAppointmentDoc: function () {
    return Appointments.findOne({_id: this._id});
  }
});

Template.cancelAppointment.events({
  //add your events here
});

Template.cancelAppointment.onCreated(function () {
  //add your statement here
});

function hideModals() {
  var $modal = $('.confirm.cancel.modal');
  $modal.modal('hide');
  $('.modals').remove();
}

Template.cancelAppointment.onRendered(function () {
  $('.confirm.cancel.modal').modal({
    closable: true,
    onShow: function () {
    },
    onHidden: function (argument) {
      $('.confirm.cancel.modal').remove();
    },
    onHide: function () {
    },
    onApprove: function () {
    },
    onDeny: function (argument) {
      hideModals();
    }
  }).modal('show');
});

Template.cancelAppointment.onDestroyed(function () {
  //add your statement here
});

var hooksObject = {
  onSuccess: function (formType, result) {
    if (result) {
      hideModals();
    }
      // MDL snackbar call to create new patient, maybe this can be simplified?
      var notificationCancelAppointment = document.querySelector('.mdl-js-snackbar');
      notificationCancelAppointment.MaterialSnackbar.showSnackbar(
        {
          message: 'Appointment Cancelled'
        }
      );
  }
};
AutoForm.addHooks('updateAppointmentsForm', hooksObject, true);

