Template.appointmentForm.helpers({
  formType: function () {
    return this.apptId ? "update" : "insert";
  },
  selectedListDoc: function () {
    return Appointments.findOne(this.apptId);
  }
});

Template.appointmentForm.onRendered(function () {
  $('.listForm').find('select').dropdown();
});

var hooksObject = {
  onSuccess: function (formType, result) {
    Router.go('/appointments/?today=true');
    var notificationAppointmentCreated = document.querySelector('.mdl-js-snackbar');
    notificationAppointmentCreated.MaterialSnackbar.showSnackbar(
  {
    message: 'Appointment created'
  }
);
  }
};
AutoForm.addHooks('insertAppointmentsForm', hooksObject, true);
