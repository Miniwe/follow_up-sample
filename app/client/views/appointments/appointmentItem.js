Template.appointmentItem.helpers({
  checkedIn: function(){
    return false;
    // return Admissions.findOne({
    //     appointmentId: this._id,
    //     checkedIn: {$ne: null}
    // });
  }
});

Template.appointmentItem.events({
  "click .cancelAppointment": function(event, template) {
    Blaze.renderWithData(Template.cancelAppointment, template.data, $('body')[0]);
  },
  "click .js-admissions-modal": function(event, template) {
    event.preventDefault();
    Blaze.renderWithData(Template.admissionsModal, template.data, $('body')[0]);
  }
});

Template.appointmentItem.onCreated(function () {
  //add your statement here
});

Template.appointmentItem.onRendered(function () {
  //add your statement here
});

Template.appointmentItem.onDestroyed(function () {
  //add your statement here
});

