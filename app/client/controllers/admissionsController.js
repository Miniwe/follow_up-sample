admissionsController = applicationController.extend({
  layoutTemplate: 'applicationLayout',
 
  // waitOn: function () {
  //   return [
  //     Meteor.subscribe('diseases.predefined'),
  //   ]
  // },
  _getAdmission: function (params) {
      var id = (params._id) ? params._id : '';
      return Admissions.findOne({_id: id});
  },
  new: function () {
    let patient, patientId, data = {};
    if (patientId = Router.current().params.query.patientId) {
      if (patient = Patients.findOne(patientId)) {
        data.admission = {
          patientId: patient._id,
          careplanId: Careplans.findOne({patientId: patient._id})._id,
          checkedIn: moment().format("YYYY-MM-DDTHH:mm")
        }
      }
    }
    this.render('admissionNew', {data: data});
  },
  edit: function () {
    var params = this.params;
    this.render('admissionEdit', { data: {
      admission: this._getAdmission(params)
    }});
  },
  view: function () {
    var params = this.params;
    this.render('admission', { data: {
      admission: this._getAdmission(params)

    }});
  },
  discharge: function () {
    var params = this.params;
    let admission = this._getAdmission(params);
    admission.checkedOut = moment().format("YYYY-MM-DDTHH:mm");
    this.render('admissionFormUpdate', { data: {
      admission: admission
    }});
  },
  action: function () {
    var params = this.params;
    this.render('admissions', {
      data: {
        admissions: function() {
          return Admissions.list(params.list).fetch();
        }
      }
    });
  }
});


admissionsNewController = applicationController.extend({
  layoutTemplate: 'applicationLayout',
  // waitOn: function () {
  //   return [
  //     Meteor.subscribe('admissions')
  //   ]
  // },
  new: function () {
    let patient, patientId, data = {};
    if (patientId = Router.current().params.query.patientId) {
      if (patient = Patients.findOne(patientId)) {
        data.admission = {
          patientId: patient._id,
          careplanId: Careplans.findOne({patientId: patient._id})._id,
          checkedIn: moment().format("YYYY-MM-DDTHH:mm")
        }
      }
    }
    this.render('admissionNew', {data: data});
  }
});
