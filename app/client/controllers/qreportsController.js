reportsController = applicationController.extend({
  data() {
    return {
      params: {}
    }
  },
  viewAction() {
    this.render('report', { data: {
      // report: this._getReport(params)
    }});
  },
  listAction() {
    this.render('reports', { data: {
      // report: this._getReport(params)
    }});
  },
  exportAction() {
    this.render('reports_export', { data: {
      // report: this._getReport(params)
    }});
  },
  action() {
      this.listAction();
  }
});
