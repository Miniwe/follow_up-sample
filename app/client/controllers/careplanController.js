var careplan;
careplanController = applicationController.extend({
  waitOn: function () {
    careplan = Careplans.findOne({
      _id: this.params._id
    });
    return {
      ready: function () {
        return (careplan != undefined);
      }
    }
  },
  action: function () {
    var item = careplan;
    if (item) {
      item.process();
    }
    var physiciansQuestionnaires = Questionnaires.find({
      careplanId: item._id,
      assignedTo: Meteor.userId()
    }).fetch();

    var patientsQuestionnaires = Questionnaires.find({
      careplanId: item._id,
      assignedTo: item.patientId
    }).fetch();

    var forms = this.params.query["forms"];

    if (forms) {
      var questionnaire = Questionnaires.findOne({
        careplanId: this.params._id,
        type: forms
      });

      if (questionnaire) {
        Router.go('/questionnaires/' + questionnaire._id);
        return;
      }

    }
    this.render('careplan', {
      data: {
        careplan: item,
        physiciansQuestionnaires: physiciansQuestionnaires,
        patientsQuestionnaires: patientsQuestionnaires
      }
    });
  }
});
