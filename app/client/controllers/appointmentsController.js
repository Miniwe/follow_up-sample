var forms;
appointmentsController = applicationController.extend({
  action: function () {
    var params = this.params;
    var query = params.query;
    var filter = {
      today: query["today"] === "true",
      tomorrow: query["tomorrow"] === "true",
      nextweek: query["nextweek"] === "true",
      completed: query["completed"] === "true"
    };
    this.render('appointments', {
      data: function () {
        return {
          appointments: Appointments.filter(filter),
          filter: filter
        }
      }
    });
  }
});
