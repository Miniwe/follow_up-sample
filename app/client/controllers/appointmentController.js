appointmentController = applicationController.extend({
  onBeforeAction: function() {
    var self = this;
    var appt = this.appt = Appointments.findOne({
      _id: this.params._id
    });
    this.next();
  },
  action: function() {
    this.render("editAppointment", {
      data: function() {
        return {
          appt: this.appt,
          apptId: this.params._id
        }
      }
    });
  }
});
